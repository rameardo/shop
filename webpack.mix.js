const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.sass('resources/sass/back.scss', 'public/cdn/css/back/back.css')
mix.sass('resources/sass/back.scss', 'public/cdn/css/back/main.css')
   // .sass('resources/sass/back.scss', 'public/cdn/css/back/main.css');
   .scripts([
   		'public/cdn/back/assets/js/bundle.js',
   		'public/cdn/back/assets/js/scripts.js',
   		'public/cdn/back/assets/js/charts/chart-crypto.js',
   		'public/cdn/back/ajax/ajax.js'
   	], 'public/cdn/js/back/main.js');