<!DOCTYPE html>
<html>
	<head>

		<title>{{thisApp('name')}}</title>

		@include('back.assets._css')



	</head>
	<body class="nk-body bg-white npc-general pg-error">

		<div class="nk-app-root">
	        <!-- wrap @s -->
	        <div class="nk-wrap justify-center">

	            <!-- content @s -->
	            <div class="nk-content nk-content-lg nk-content-fluid">
	                <div class="container-xl wide-lg">
	                    <div class="nk-content-inner">
	                        <div class="nk-content-body">
	                            <div class="kyc-app wide-sm m-auto">
	                                <div class="nk-block-head nk-block-head-lg wide-xs mx-auto">
	                                    <div class="nk-block-head-content text-center">
	                                        <h2 class="nk-block-title fw-normal">{{thisApp('name')}} Installation</h2>
	
	                                    </div>
	                                </div><!-- .nk-block-head -->
	                                <div class="nk-block">
	                                    <div class="card card-bordered">
	                                        <div class="card-inner card-inner-lg">
	                                            <div class="nk-kyc-app p-sm-2 text-center">
	                                                <div class="nk-kyc-app-icon">
	                                                    <img src="{{asset('cdn/back/images/icons/support-payment.svg')}}" style="width:100px;">
	                                                </div>
	                                                <div class="nk-kyc-app-text mx-auto">
	                                                    <p class="lead">
	                                                    	Before starting, it appears that the application is not installed or configured to run, you must configure the settings and install it.
	                                                    </p>
	                                                </div>
	                                                <div class="nk-kyc-app-action">
	                                                    <a href="{{route('install')}}" class="btn btn-lg btn-primary">Start Installation</a>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div><!-- .card -->
	                                    <div class="text-center pt-4">
	                                        <p>If you have any question, please contact our support team <a href="mailto:{{thisApp('support_email')}}">{{thisApp('support_email')}}</a></p>
	                                    </div>
	                                </div> <!-- .nk-block -->
	                            </div><!-- .kyc-app -->
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <!-- content @e -->

	        </div>
	        <!-- wrap @e -->
	    </div>

		@include('back.assets._js')


	</body>
</html>
