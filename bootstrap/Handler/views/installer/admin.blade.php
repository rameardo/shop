<!DOCTYPE html>
<html>
    <head>

        @include ('back.assets._meta')

        <title>Admin - @yield('title')</title>

        @include('back.assets._css')

        @yield('css')

        @yield('js_header')


    </head>
    <body class="nk-body npc-crypto has-sidebar has-sidebar-fat ui-clean">
{{-- {{dd(Session::get('seed_data'))}} --}}
    <div class="nk-app-root">
        <!-- wrap @s -->
        <div class="nk-wrap ">
            <form class="form-validate is-alter main-form" novalidate="novalidate" action="{{route('install.admin.store')}}" method="POST" data-select2-id="6">
                @csrf
                <!-- content @s -->
                <div class="nk-content nk-content-lg nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-lg wide-xs mx-auto">
                                    <div class="nk-block-head-content text-center">
                                        <h2 class="nk-block-title fw-normal">{{thisApp('name')}} INSTALLATION</h2>
                                        <div class="nk-block-des">
                                            <p>Welcome to <strong>{{thisApp('name')}}</strong>. You are few steps away to complete your installation. These are required to run this application! Let’s start!</p>
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="nk-block-head" style="padding-bottom: 1.5rem;">
                                    <div class="nk-block-between-md g-2">
                                        <div class="nk-block-head-content">
                                            <ul class="nk-block-tools gx-3">
                                                <li>
                                                    <a href="{{route('install.database')}}" class="btn btn-primary">
                                                        <em class="icon ni ni-arrow-long-left"></em>
                                                        <span>Pervious</span>
                                                    </a>

                                                </li>
                                            </ul>
                                            
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <ul class="nk-block-tools gx-3">
                                                <li>
                                                    <button type="submit" class="btn btn-primary">
                                                        <span>Setup</span>
                                                        <em class="icon ni ni-arrow-long-right"></em>
                                                    </button>

                                                </li>
                                            </ul>
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div>

                                <div class="nk-block">
                                    <div class="card card-custom-s1 card-bordered">
                                        <div class="row no-gutters">
                                            <div class="col-lg-4 card-inner-group">
                                                <div class="">
                                                    <div class="card-inner">
                                                        <h5>Let’s Finish Installation</h5>
                                                        <p>Only few minutes required to complete your basic Project information.</p>
                                                    </div>
                                                    <div class="card-inner">
                                                        <ul class="list list-step">
                                                            <li class="list-step-done">Start Installation</li>
                                                            <li class="list-step-done">Server Requirements</li>
                                                            <li class="list-step-done">Basic Information</li>
                                                            <li class="list-step-done">Install Database</li>
                                                            <li class="list-step-current">Admin Account</li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div><!-- .col -->
                                            <div class="col-lg-8">
                                                <div class="card-inner card-inner-lg h-100">
                                                    <div class="align-center flex-wrap flex-md-nowrap g-3">


                                                            <div class="nk-block-content col-lg-12">

                                                                <div class="nk-block-content-head">
                                                                    <h4>Create Admin Account</h4>
                                                                </div>

                                                                <div class="row g-4">


                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="fva-first_name">First Name</label>
                                                                            <div class="form-control-wrap">
                                                                                <input type="text" class="form-control" id="fva-first_name" name="first_name" placeholder="John" value="{{_sessionValue('first_name')}}" required>
                                                                                @error('first_name')
                                                                                    <span id="fva-first_name-error" class="invalid">{{ $message }}</span>
                                                                                @enderror                                                                                  
                                                                            </div>
                                                                        </div>
                                                                    </div>


 

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="fva-last_name">Last Name</label>
                                                                            <div class="form-control-wrap">
                                                                                <input type="text" class="form-control" id="fva-last_name" name="last_name" placeholder="Doe" value="{{_sessionValue('last_name')}}" required>
                                                                                @error('last_name')
                                                                                    <span id="fva-last_name-error" class="invalid">{{ $message }}</span>
                                                                                @enderror                                                                                  
                                                                            </div>
                                                                        </div>
                                                                    </div>



 

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="fva-email_address">E-Mail Address</label>
                                                                            <div class="form-control-wrap">
                                                                                <input type="email" class="form-control" id="fva-email_address" name="email_address" placeholder="{{ 'john.doe@example.com'}}" value="{{_sessionValue('email_address')}}" required>
                                                                                @error('email_address')
                                                                                    <span id="fva-email_address-error" class="invalid">{{ $message }}</span>
                                                                                @enderror                                                                                  
                                                                            </div>
                                                                        </div>
                                                                    </div>


 

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="fva-password">Password</label>
                                                                            <div class="form-control-wrap">
                                                                                <input type="password" class="form-control" id="fva-password" name="password" placeholder="●●●●●●●●" value="{{_sessionValue('password')}}" required>
                                                                                @error('password')
                                                                                    <span id="fva-password-error" class="invalid">{{ $message }}</span>
                                                                                @enderror                                                                                  
                                                                            </div>
                                                                        </div>
                                                                    </div>





                                                                    <div class="col-md-4" style="margin-top: -10px;">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="fva-maintenance_mode">Maintenance Mode</label>
                                                                            <div class="form-control-wrap">
                                                                                 <select class="form-control form-select" id="fva-maintenance_mode" data-placeholder="Select a option" name="maintenance_mode[]" required>
                                                                                        <option label="empty" value=""></option>
                                                                                        <option value="0" {{ _sessionSelected('maintenance_mode', 0) }}>Disable</option>
                                                                                        <option value="1" {{ _sessionSelected('maintenance_mode', 1) }}>Enable</option>
                                                                                </select>
                                                                                @error('maintenance_mode')
                                                                                    <span id="fva-maintenance_mode-error" class="invalid">{{ $message }}</span>
                                                                                @enderror                                                                                 
                                                                            </div>
                                                                        </div>
                                                                    </div>






                                                                    <div class="col-md-4" style="margin-top: -10px;">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="fva-registration">Registration in App?</label>
                                                                            <div class="form-control-wrap">
                                                                                 <select class="form-control form-select" id="fva-registration" data-placeholder="Select a option" name="registration[]" required>
                                                                                        <option label="empty" value=""></option>
                                                                                        <option value="0" {{ _sessionSelected('registration', 0) }}>Disable</option>
                                                                                        <option value="1" {{ _sessionSelected('registration', 1) }}>Enable</option>
                                                                                </select>
                                                                                @error('registration')
                                                                                    <span id="fva-registration-error" class="invalid">{{ $message }}</span>
                                                                                @enderror                                                                                 
                                                                            </div>
                                                                        </div>
                                                                    </div>






                                                                    <div class="col-md-4" style="margin-top: -10px;">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="fva-seed_data">Seed Default Data</label>
                                                                            <div class="form-control-wrap">
                                                                                 <select class="form-control form-select" id="fva-seed_data" data-placeholder="Select a option" name="seed_data[]" required>
                                                                                        <option label="empty" value=""></option>
                                                                                        <option value="0" {{ _sessionSelected('seed_data', 0) }}>No</option>
                                                                                        <option value="1" {{ _sessionSelected('seed_data', 1) }}>Yes</option>
                                                                                </select>
                                                                                @error('seed_data')
                                                                                    <span id="fva-seed_data-error" class="invalid">{{ $message }}</span>
                                                                                @enderror                                                                                 
                                                                            </div>
                                                                        </div>
                                                                    </div>






                                
                                                                </div>

                                                            </div>                            
                                                    </div>
                                                </div>
                                            </div><!-- .col -->
                                        </div><!-- .row -->
                                    </div><!-- .card -->
                                </div><!-- .nk-block -->
                                <div class="nk-block">
                                    <div class="card card-bordered">
                                        <div class="card-inner card-inner-lg">
                                            <div class="align-center flex-wrap flex-md-nowrap g-4">
                                                <div class="nk-block-image w-120px flex-shrink-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 118">
                                                        <path d="M8.916,94.745C-.318,79.153-2.164,58.569,2.382,40.578,7.155,21.69,19.045,9.451,35.162,4.32,46.609.676,58.716.331,70.456,1.845,84.683,3.68,99.57,8.694,108.892,21.408c10.03,13.679,12.071,34.71,10.747,52.054-1.173,15.359-7.441,27.489-19.231,34.494-10.689,6.351-22.92,8.733-34.715,10.331-16.181,2.192-34.195-.336-47.6-12.281A47.243,47.243,0,0,1,8.916,94.745Z" transform="translate(0 -1)" fill="#f6faff" />
                                                        <rect x="18" y="32" width="84" height="50" rx="4" ry="4" fill="#fff" />
                                                        <rect x="26" y="44" width="20" height="12" rx="1" ry="1" fill="#e5effe" />
                                                        <rect x="50" y="44" width="20" height="12" rx="1" ry="1" fill="#e5effe" />
                                                        <rect x="74" y="44" width="20" height="12" rx="1" ry="1" fill="#e5effe" />
                                                        <rect x="38" y="60" width="20" height="12" rx="1" ry="1" fill="#e5effe" />
                                                        <rect x="62" y="60" width="20" height="12" rx="1" ry="1" fill="#e5effe" />
                                                        <path d="M98,32H22a5.006,5.006,0,0,0-5,5V79a5.006,5.006,0,0,0,5,5H52v8H45a2,2,0,0,0-2,2v4a2,2,0,0,0,2,2H73a2,2,0,0,0,2-2V94a2,2,0,0,0-2-2H66V84H98a5.006,5.006,0,0,0,5-5V37A5.006,5.006,0,0,0,98,32ZM73,94v4H45V94Zm-9-2H54V84H64Zm37-13a3,3,0,0,1-3,3H22a3,3,0,0,1-3-3V37a3,3,0,0,1,3-3H98a3,3,0,0,1,3,3Z" transform="translate(0 -1)" fill="#798bff" />
                                                        <path d="M61.444,41H40.111L33,48.143V19.7A3.632,3.632,0,0,1,36.556,16H61.444A3.632,3.632,0,0,1,65,19.7V37.3A3.632,3.632,0,0,1,61.444,41Z" transform="translate(0 -1)" fill="#6576ff" />
                                                        <path d="M61.444,41H40.111L33,48.143V19.7A3.632,3.632,0,0,1,36.556,16H61.444A3.632,3.632,0,0,1,65,19.7V37.3A3.632,3.632,0,0,1,61.444,41Z" transform="translate(0 -1)" fill="none" stroke="#6576ff" stroke-miterlimit="10" stroke-width="2" />
                                                        <line x1="40" y1="22" x2="57" y2="22" fill="none" stroke="#fffffe" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                                        <line x1="40" y1="27" x2="57" y2="27" fill="none" stroke="#fffffe" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                                        <line x1="40" y1="32" x2="50" y2="32" fill="none" stroke="#fffffe" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                                        <line x1="30.5" y1="87.5" x2="30.5" y2="91.5" fill="none" stroke="#9cabff" stroke-linecap="round" stroke-linejoin="round" />
                                                        <line x1="28.5" y1="89.5" x2="32.5" y2="89.5" fill="none" stroke="#9cabff" stroke-linecap="round" stroke-linejoin="round" />
                                                        <line x1="79.5" y1="22.5" x2="79.5" y2="26.5" fill="none" stroke="#9cabff" stroke-linecap="round" stroke-linejoin="round" />
                                                        <line x1="77.5" y1="24.5" x2="81.5" y2="24.5" fill="none" stroke="#9cabff" stroke-linecap="round" stroke-linejoin="round" />
                                                        <circle cx="90.5" cy="97.5" r="3" fill="none" stroke="#9cabff" stroke-miterlimit="10" />
                                                        <circle cx="24" cy="23" r="2.5" fill="none" stroke="#9cabff" stroke-miterlimit="10" /></svg>
                                                </div>
                                                <div class="nk-block-content">
                                                    <div class="nk-block-content-head px-lg-4">
                                                        <h5>We’re here to help you!</h5>
                                                        <p class="text-soft">Ask a question or file a support ticket, manage request, report an issues. Our team support team will get back to you by email.</p>
                                                    </div>
                                                </div>
                                                <div class="nk-block-content flex-shrink-0">
                                                    <a href="#" class="btn btn-lg btn-outline-primary">Get Support Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .card -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
            </form>
        </div>
        <!-- wrap @e -->
    </div>


        @include('back.assets._js')

        @yield('js_footer')
        @yield('js_error_code')
        @yield('js_success_code')

    </body>
</html>