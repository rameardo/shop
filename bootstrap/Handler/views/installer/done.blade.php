<!DOCTYPE html>
<html>
    <head>

        @include ('back.assets._meta')

        <title>Admin - @yield('title')</title>

        @include('back.assets._css')

        @yield('css')

        @yield('js_header')

        <link rel="stylesheet" type="text/css" href="{{asset('css/installation.css')}}">


    </head>
    <body class="nk-body npc-crypto has-sidebar has-sidebar-fat ui-clean">

    <div class="nk-app-root">
        <!-- wrap @s -->
        <div class="nk-wrap ">
            <form class="form-validate is-alter main-form" novalidate="novalidate" action="{{route('install.admin.store')}}" method="POST" data-select2-id="6">
                @csrf
                <!-- content @s -->
                <div class="nk-content nk-content-lg nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-lg wide-xs mx-auto">
                                    <div class="nk-block-head-content text-center">
                                        <h2 class="nk-block-title fw-normal">{{thisApp('name')}} INSTALLATION</h2>
                                        <div class="nk-block-des">
                                            <p>Welcome to <strong>{{thisApp('name')}}</strong>. You are few steps away to complete your installation. These are required to run this application! Let’s start!</p>
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->
           
                                <div class="nk-block">
                                    <div class="card card-custom-s1 card-bordered">
                                        <div class="row no-gutters">

                                            <div class="col-lg-12">
                                                <div class="card-inner card-inner-lg h-100 position-relative">
                                                    <div class="align-center flex-wrap flex-md-nowrap g-3">


                                                            <div class="nk-block-content col-lg-12">

                                                                <div class="nk-block-content-head">
                                                                    <h4>Finish Installation</h4>
                                                                    <span class="sub-text sub-text-sm text-soft">

                                                                        <span class="text-danger fw-bold">Attention</span>: Do not close the page until the entire project is installed! Upon completion of the project, you will be automatically transferred to the home page
                                                                    </span>
                                                                </div>

                                                                <div class="row g-4">

                                                                    <div class="setup row">

                        
                                                                        <div class="left col-md-6">

                                                                            <div class="row">

                                                                                <div class="col-md-6">
                                                                                    <ul class="check_action">
                                                                                        <li id="create_backup"> Create a backup </li>
                                                                                        <li id="initialize_project"> Initialize the project </li>
                                                                                        <li id="database_connection"> Check Database Connection  </li>
                                                                                        <li id="database_creation">Database creation </li>
                                                                                    </ul>
                                                                                </div>

                                                                                <div class="col-md-6">
                                                                                    <ul class="check_action">
                                                                                        <li id="seeding_database"> Seeding Data to Database </li>
                                                                                        <li id="admin_account"> Admin account creation </li>
                                                                                        <li id="filte_inputs">Filter Inputs Data</li>
                                                                                        <li id="check_role"> Check Role Privacy </li>
                                                                                    </ul>
                                                                                </div>

                                                                            </div>

                                                                        </div>

                                                                        <div class="right col-md-6">

                                                                            <h4 class="fw-light">Hey {{$first_name}}! Please be Patient!</h4>
                                                                            <span class="sub-text sub-text-sm text-soft">
                                                                                It will take some time to install the project, please be patient
                                                                            </span>


                                                                            <div class="mt-2 progress-responsive">
                                                                                <figure clas="progress-figure">
                                                                                    <div class="progress-responsive__bar"></div>
                                                                                </figure>
                                                                            </div><!-- .progress-responsive -->

                                                                            <div class="row status">
                                                                                <div class="col-md-10">
                                                                                    <p class="action_status" id="word">
                                                                                        Crafting {{$app_name}}...
                                                                                    </p>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <p class="progress-responsive__percent text-right fw-500"></p>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        </div>

                                                                    </div>
                                                                    
                                                                </div>

                                                            </div>                            
                                                    </div>
                                                </div>
                                            </div><!-- .col -->
                                        </div><!-- .row -->
                                    </div><!-- .card -->
                                </div><!-- .nk-block -->
                                <div class="nk-block">
                                    <div class="card card-bordered">
                                        <div class="card-inner card-inner-lg">
                                            <div class="align-center flex-wrap flex-md-nowrap g-4">
                                                <div class="nk-block-image w-120px flex-shrink-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 118">
                                                        <path d="M8.916,94.745C-.318,79.153-2.164,58.569,2.382,40.578,7.155,21.69,19.045,9.451,35.162,4.32,46.609.676,58.716.331,70.456,1.845,84.683,3.68,99.57,8.694,108.892,21.408c10.03,13.679,12.071,34.71,10.747,52.054-1.173,15.359-7.441,27.489-19.231,34.494-10.689,6.351-22.92,8.733-34.715,10.331-16.181,2.192-34.195-.336-47.6-12.281A47.243,47.243,0,0,1,8.916,94.745Z" transform="translate(0 -1)" fill="#f6faff" />
                                                        <rect x="18" y="32" width="84" height="50" rx="4" ry="4" fill="#fff" />
                                                        <rect x="26" y="44" width="20" height="12" rx="1" ry="1" fill="#e5effe" />
                                                        <rect x="50" y="44" width="20" height="12" rx="1" ry="1" fill="#e5effe" />
                                                        <rect x="74" y="44" width="20" height="12" rx="1" ry="1" fill="#e5effe" />
                                                        <rect x="38" y="60" width="20" height="12" rx="1" ry="1" fill="#e5effe" />
                                                        <rect x="62" y="60" width="20" height="12" rx="1" ry="1" fill="#e5effe" />
                                                        <path d="M98,32H22a5.006,5.006,0,0,0-5,5V79a5.006,5.006,0,0,0,5,5H52v8H45a2,2,0,0,0-2,2v4a2,2,0,0,0,2,2H73a2,2,0,0,0,2-2V94a2,2,0,0,0-2-2H66V84H98a5.006,5.006,0,0,0,5-5V37A5.006,5.006,0,0,0,98,32ZM73,94v4H45V94Zm-9-2H54V84H64Zm37-13a3,3,0,0,1-3,3H22a3,3,0,0,1-3-3V37a3,3,0,0,1,3-3H98a3,3,0,0,1,3,3Z" transform="translate(0 -1)" fill="#798bff" />
                                                        <path d="M61.444,41H40.111L33,48.143V19.7A3.632,3.632,0,0,1,36.556,16H61.444A3.632,3.632,0,0,1,65,19.7V37.3A3.632,3.632,0,0,1,61.444,41Z" transform="translate(0 -1)" fill="#6576ff" />
                                                        <path d="M61.444,41H40.111L33,48.143V19.7A3.632,3.632,0,0,1,36.556,16H61.444A3.632,3.632,0,0,1,65,19.7V37.3A3.632,3.632,0,0,1,61.444,41Z" transform="translate(0 -1)" fill="none" stroke="#6576ff" stroke-miterlimit="10" stroke-width="2" />
                                                        <line x1="40" y1="22" x2="57" y2="22" fill="none" stroke="#fffffe" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                                        <line x1="40" y1="27" x2="57" y2="27" fill="none" stroke="#fffffe" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                                        <line x1="40" y1="32" x2="50" y2="32" fill="none" stroke="#fffffe" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                                        <line x1="30.5" y1="87.5" x2="30.5" y2="91.5" fill="none" stroke="#9cabff" stroke-linecap="round" stroke-linejoin="round" />
                                                        <line x1="28.5" y1="89.5" x2="32.5" y2="89.5" fill="none" stroke="#9cabff" stroke-linecap="round" stroke-linejoin="round" />
                                                        <line x1="79.5" y1="22.5" x2="79.5" y2="26.5" fill="none" stroke="#9cabff" stroke-linecap="round" stroke-linejoin="round" />
                                                        <line x1="77.5" y1="24.5" x2="81.5" y2="24.5" fill="none" stroke="#9cabff" stroke-linecap="round" stroke-linejoin="round" />
                                                        <circle cx="90.5" cy="97.5" r="3" fill="none" stroke="#9cabff" stroke-miterlimit="10" />
                                                        <circle cx="24" cy="23" r="2.5" fill="none" stroke="#9cabff" stroke-miterlimit="10" /></svg>
                                                </div>
                                                <div class="nk-block-content">
                                                    <div class="nk-block-content-head px-lg-4">
                                                        <h5>We’re here to help you!</h5>
                                                        <p class="text-soft">Ask a question or file a support ticket, manage request, report an issues. Our team support team will get back to you by email.</p>
                                                    </div>
                                                </div>
                                                <div class="nk-block-content flex-shrink-0">
                                                    <a href="#" class="btn btn-lg btn-outline-primary">Get Support Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .card -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
            </form>
        </div>
        <!-- wrap @e -->
    </div>



    <div id="success"></div>


        @include('back.assets._js')
        <script src="{{asset('js/installation.js')}}"></script>
        @yield('js_footer')
        @yield('js_error_code')
        @yield('js_success_code')

        <script type="text/javascript">

            function setupStatus(key, duration, progress_status, element, status = '')
            {
                var halfDuration = duration / 2;
                var fastAll = duration * 0.75;

                $(element).css('opacity', 1);

                $(element).prepend('<span class="spinner-border spinner-border-sm ac_icon"></span>');

                BG.init = function() {
                  BG.fixed(progress_status, duration);  // Percentage, duration
                  BG.responsive(progress_status, duration);  // Percentage, duration
                };    

                var attachText = setTimeout(function () {

                    $("#word").fadeOut(100, function () {
                        $(this).text(wordsArray[key]).fadeIn(100);
                        $(element + ' span' ).remove();
     

                        switch(status)
                        {
                            case 1:
                                 $(element).prepend('<em class="icon ni ni-check-circle-fill text-success"></em>');
                                 break;
                            case 0:
                                 $(element).prepend('<em class="icon ni ni-cross-circle-fill text-danger"></em>');
                                 break;     
                            default:
                                $(element).prepend('<em class="icon ni ni-alert-fill text-warning" ></em>');    
                                $(element + ' em').attr('data-toggle', 'popover');    
                                $(element + ' em').attr('title', 'Popover title');    
                                $(element + ' em').attr('data-content', 'And heres some amazing content');    
                        }
                        
                         
                        // alert('hello');
                        // $(this).text(wordsArray[count % wordsArray.length]).fadeIn(100);
                    });

                }, duration);

            }

                    
            $(function () {

                BG.init = function() {
                  BG.fixed(1, 0);  // Percentage, duration
                  BG.responsive(1, 0);  // Percentage, duration
                };    
    

                wordsArray = [
                    "Initializing the {{$db_name}} project to boot and install default package ", 
                    "Set {{$environment}} environment", 
                    "Checking Database Connection {{$db_hostname . ':' . $db_port}}", 
                    "Creating {{$db_name}} Database",
                    "Seeding Data to {{$db_name}}",
                    "Create an account for {{$email_address}}",
                    "Filter Providet Data",
                    "Finishing installation..."
                ];


                setupStatus(0, 1000, 12, '#create_backup', 1);
                setupStatus(1, 2000, 15, '#initialize_project', 1);
                setupStatus(2, 1500, 18, '#database_connection', {{$db_status === true ? 1 : 0}});
                setupStatus(3, 4000, 24, '#database_creation', {{$db_creation === true ? 1 : 0}});


                setupStatus(4, 5000, 33, '#seeding_database', 1);
                setupStatus(5, 1300, 47, '#admin_account', 1);
                setupStatus(6, 7000, 89, '#filte_inputs', 1);
                setupStatus(7, 8000, 100, '#check_role', 1);

                setTimeout(function(){
                    $('#success').load("{{asset('redirect.html')}}");
                }, 10000);

                var db_creation = {{$db_creation}};
                if (db_creation == 1)
                {
                    setTimeout(function(){
                        window.location.href = "{{route('install.migrate')}}";
                    }, 11000);                    
                }
                
                BG.init(); 
            });            
        </script>

    </body>
</html>

