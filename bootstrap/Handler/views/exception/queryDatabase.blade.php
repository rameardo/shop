<!DOCTYPE html>
<html>
	<head>

		<title>{{thisApp('name')}} Handler | Database</title>

		@include('back.assets._css')



	</head>

	<body class="nk-body bg-white npc-general pg-error">
	    <div class="nk-app-root">
	        <!-- main @s -->
	        <div class="nk-main ">
	            <!-- wrap @s -->
	            <div class="nk-wrap justify-center">
	                <!-- content @s -->
	                <div class="nk-content ">
	                    <div class="nk-block nk-block-middle wide-md mx-auto">
	                        <div class="nk-block-content nk-error-ld text-center">
	                            
	                            <div class="wide-xs mx-auto text-left">
	                                <h3 class="nk-error-title">Database Dupplicattion Error</h3>

	                                <p class="nk-error-text">
	                                	{{$exception->getMessage()}}
	                                </p>
	                                <p class="sub-text-sm">Error code: {{errorCode('database.dupplicate')}}</p>
	                                <a href="{{route('install.database')}}" class="btn btn-sm btn-light">
	                                	Change Database name
	                                </a>
	                            </div>
	                        </div>
	                    </div><!-- .nk-block -->
	                </div>
	                <!-- wrap @e -->
	            </div>
	            <!-- content @e -->
	        </div>
	        <!-- main @e -->
	    </div>

		@include('back.assets._js')


	</body>


</html>
