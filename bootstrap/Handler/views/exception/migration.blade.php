<!DOCTYPE html>
<html>
	<head>

		<title>{{thisApp('name')}} Handler | Database Migration</title>

		@include('back.assets._css')

		@php

		// if (request()->post())
		// {
		// 	\Artisan::call('migrate:refresh');
		// }
		if ($_SERVER['REQUEST_METHOD'] === 'GET'):
			if (isset($_GET['migration'])):
				\Artisan::call('migrate:refresh');
				\Artisan::call('db:seed');
				return redirect('/');
			endif;
		endif;

		@endphp


	</head>

	<body class="nk-body bg-white npc-general pg-error">
	    <div class="nk-app-root">
	        <!-- main @s -->
	        <div class="nk-main ">
	            <!-- wrap @s -->
	            <div class="nk-wrap justify-center">
	                <!-- content @s -->
	                <div class="nk-content ">
	                    <div class="nk-block nk-block-middle wide-md mx-auto">
	                        <div class="nk-block-content nk-error-ld text-center">
	                            
	                            <div class="wide-xs mx-auto text-left">
	                                <h3 class="nk-error-title">Database Migration Error</h3>

	                                <p class="nk-error-text">
	                                	Your Database is not Migrated! App can't Run before migrating Database.
	                                </p>
	                                <p class="sub-text-sm">Error code: {{errorCode('database.notMigrated')}}</p>
	                                <form method="GET">
	                                	<input type="hidden" name="migration" value="1">
		                                <button  id="migrate" class="btn btn-sm btn-light">
		                                	Run Migration
		                                </button>
	                                </form>
	                            </div>
	                        </div>
	                    </div><!-- .nk-block -->
	                </div>
	                <!-- wrap @e -->
	            </div>
	            <!-- content @e -->
	        </div>
	        <!-- main @e -->
	    </div>

		@include('back.assets._js')



	</body>


</html>
