<?php

use Illuminate\Database\Seeder;

class DefaultAppSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin Prefix
        createSetting('admin_prefix', 'This is Admin Prefix', 'admin');


        // Default Site Language
        DB::table('app_settings')->insert([
            'name' => 'Language',
            'description' => 'This is Site Default Language',
            'value' => 'en',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
    }
}
