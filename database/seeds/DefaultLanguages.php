<?php

use Illuminate\Database\Seeder;

class DefaultLanguages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::table('languages')->insert([
                'name' => 'English',
                'shortcut' => 'en',
                'code' => 'en_us',
                'icon_code' => 'flag-icon flag-icon-us',
                'position' => 1,
                'status'    => 1,
                'dirction'  => 'ltr',
                'created_at'    => now(),
                'updated_at'    => now(),
            ]);            
        } catch (\Exception $e) {
            $message = "English Language is already exists in your (" . env('DB_DATABASE') . ') Database.';
            throw new App\Exceptions\DupplicateException($message, 500);            
        }

    }
}
