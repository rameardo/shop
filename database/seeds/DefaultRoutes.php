<?php

use Illuminate\Database\Seeder;

class DefaultRoutes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // createRoute($key, $value);
        createRoute('get', '/', 'Back\PagesController', 'Dashboard', 'admin', adminPrefix());
        // createRoute('admin', 'admin', 'admin');
        // createRoute('admin_settings', 'settings', 'admin');
        // createRoute('admin_categories', 'categories', 'admin');
        // createRoute('admin_categories_new', 'categories/new', 'admin');
        // createRoute('admin_categories_create', 'categories/create', 'admin');
    }
}
