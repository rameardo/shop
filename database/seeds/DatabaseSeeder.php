<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
	        // CategorySeeder::class,
            DefaultApp::class,
            DefaultLanguages::class,
            DefaultTranslation::class,
            DefaultAppSettings::class,
            DefaultRoutes::class,
	    ]);
    }
}
