<?php

use Illuminate\Database\Seeder;

class DefaultUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Site',
            'last_name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('aa11aa22'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('users')->insert([
            'first_name' => 'Site',
            'last_name' => 'Project Manager',
            'username' => 'project_manager',
            'email' => 'project_manager@gmail.com',
            'password' => Hash::make('aa11aa22'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
    }
}
