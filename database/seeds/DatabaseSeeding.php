<?php

use Illuminate\Database\Seeder;

class DatabaseSeeding extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// php artisan db:seed --class=DatabaseSeeding	
        $this->call([
	        CategorySeeder::class,
	    ]);
    }
}
