<?php

use Illuminate\Database\Seeder;
use App\Model\Pub\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Category::create([
        	'name' => 'Hot Promotions',
        	'parent_id' => null,
        	'visibility' => true,
        	'show_in_homepage' => true,
        	'show_category_image' => false,
        	'order' => 1,
        	'homepage_order' => 1,
        	'title' => 'Hot Promotions',
        	'slug' => Str::slug('Hot Promotions', '-'),
        	'description' => null
        ]);
    	
        Category::create([
        	'name' => 'Consumer Electronic',
        	'parent_id' => null,
        	'visibility' => true,
        	'show_in_homepage' => true,
        	'show_category_image' => false,
        	'order' => 1,
        	'homepage_order' => 1,
        	'title' => 'Consumer Electronic',
        	'slug' => Str::slug('Consumer Electronic', '-'),
        	'description' => null
        ]);
    	
        Category::create([
        	'name' => 'Home Audio & Theathers',
        	'parent_id' => 2,
        	'visibility' => true,
        	'show_in_homepage' => true,
        	'show_category_image' => false,
        	'order' => 1,
        	'homepage_order' => 1,
        	'title' => 'Home Audio & Theathers',
        	'slug' => Str::slug('Home Audio & Theathers', '-'),
        	'description' => null
        ]);
    	
        Category::create([
        	'name' => 'Cellphones & Accessories',
        	'parent_id' => 2,
        	'visibility' => true,
        	'show_in_homepage' => true,
        	'show_category_image' => false,
        	'order' => 1,
        	'homepage_order' => 1,
        	'title' => 'Cellphones & Accessories',
        	'slug' => Str::slug('Cellphones & Accessories', '-'),
        	'description' => null
        ]);
    	
        Category::create([
        	'name' => 'Camera, Photos & Videos',
        	'parent_id' => 2,
        	'visibility' => true,
        	'show_in_homepage' => true,
        	'show_category_image' => false,
        	'order' => 1,
        	'homepage_order' => 1,
        	'title' => 'Camera, Photos & Videos',
        	'slug' => Str::slug('Camera, Photos & Videos', '-'),
        	'description' => null
        ]);
        
        Category::create([
            'name' => 'Headphones',
            'parent_id' => 2,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Headphones',
            'slug' => Str::slug('Headphones', '-'),
            'description' => null
        ]);

        
        Category::create([
            'name' => 'Videosgames',
            'parent_id' => 2,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Videosgames',
            'slug' => Str::slug('Videosgames', '-'),
            'description' => null
        ]);

        
        Category::create([
            'name' => 'Wireless Speakers',
            'parent_id' => 2,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Wireless Speakers',
            'slug' => Str::slug('Wireless Speakers', '-'),
            'description' => null
        ]);
        
        Category::create([
            'name' => 'Office Electronic',
            'parent_id' => 2,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Office Electronic',
            'slug' => Str::slug('Office Electronic', '-'),
            'description' => null
        ]);


        Category::create([
            'name' => 'Clothing & Apparel',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Clothing & Apparel',
            'slug' => Str::slug('Clothing & Apparel', '-'),
            'description' => null
        ]);
        

        Category::create([
            'name' => 'Home, Garden & Kitchen',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Home, Garden & Kitchen',
            'slug' => Str::slug('Home, Garden & Kitchen', '-'),
            'description' => null
        ]);
                

        Category::create([
            'name' => 'Health & Beauty',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Health & Beauty',
            'slug' => Str::slug('Health & Beauty', '-'),
            'description' => null
        ]);
        

        Category::create([
            'name' => 'Yewelry & Watches',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Yewelry & Watches',
            'slug' => Str::slug('Yewelry & Watches', '-'),
            'description' => null
        ]);
        
        Category::create([
            'name' => 'Computer & Technology',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Computer & Technology',
            'slug' => Str::slug('Computer & Technology', '-'),
            'description' => null
        ]);
        
        Category::create([
            'name' => 'Computer & Tablets',
            'parent_id' => 14,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Computer & Tablets',
            'slug' => Str::slug('Computer & Tablets', '-'),
            'description' => null
        ]);
        
        
        Category::create([
            'name' => 'Laptop',
            'parent_id' => 14,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Laptop',
            'slug' => Str::slug('Laptop', '-'),
            'description' => null
        ]);
        
        
        Category::create([
            'name' => 'Monitors',
            'parent_id' => 14,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Monitors',
            'slug' => Str::slug('Monitors', '-'),
            'description' => null
        ]);
        
        
        Category::create([
            'name' => 'Networking',
            'parent_id' => 14,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Networking',
            'slug' => Str::slug('Networking', '-'),
            'description' => null
        ]);
        
            
        Category::create([
            'name' => 'Drive & Storages',
            'parent_id' => 14,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Drive & Storages',
            'slug' => Str::slug('Drive & Storages', '-'),
            'description' => null
        ]);
        
            
        Category::create([
            'name' => 'Computer Components',
            'parent_id' => 14,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Computer Components',
            'slug' => Str::slug('Computer Components', '-'),
            'description' => null
        ]);
        
                   
        Category::create([
            'name' => 'Security & Protection',
            'parent_id' => 14,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Security & Protection',
            'slug' => Str::slug('Security & Protection', '-'),
            'description' => null
        ]);
        
        
                   
        Category::create([
            'name' => 'Accessories',
            'parent_id' => 14,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Accessories',
            'slug' => Str::slug('Accessories', '-'),
            'description' => null
        ]);


        Category::create([
            'name' => 'Babies & Moms',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Babies & Moms',
            'slug' => Str::slug('Babies & Moms', '-'),
            'description' => null
        ]);
        

        Category::create([
            'name' => 'Sport & Outdoor',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Sport & Outdoor',
            'slug' => Str::slug('Sport & Outdoor', '-'),
            'description' => null
        ]);
        

        Category::create([
            'name' => 'Phones & Accessories',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Phones & Accessories',
            'slug' => Str::slug('Phones & Accessories', '-'),
            'description' => null
        ]);
        
 
        Category::create([
            'name' => 'Books & Office',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Books & Office',
            'slug' => Str::slug('Books & Office', '-'),
            'description' => null
        ]);
        
  
        Category::create([
            'name' => 'Cars & Motocycles',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Cars & Motocycles',
            'slug' => Str::slug('Cars & Motocycles', '-'),
            'description' => null
        ]);
        
       
        Category::create([
            'name' => 'Home Improments',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Home Improments',
            'slug' => Str::slug('Home Improments', '-'),
            'description' => null
        ]);
        
              
        Category::create([
            'name' => 'Vouchers & Services',
            'parent_id' => null,
            'visibility' => true,
            'show_in_homepage' => true,
            'show_category_image' => false,
            'order' => 1,
            'homepage_order' => 1,
            'title' => 'Vouchers & Services',
            'slug' => Str::slug('Vouchers & Services', '-'),
            'description' => null
        ]);
        
                     

    }
}
