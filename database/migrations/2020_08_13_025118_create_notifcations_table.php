<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotifcationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifcations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable(); // Notify just to target user
            $table->bigInteger('role_id')->nullable(); // Which role has, eg.: admin? moderator? or generic-user?
            $table->string('title')->nullable();
            $table->text('descriptions')->nullable();
            $table->text('link')->nullable();
            $table->text('image')->nullable();
            $table->text('icon')->nullable();
            $table->boolean('where')->default(false)->nullable(); // 0 is backend, 1 is frontend
            $table->boolean('status')->default(true)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifcations');
    }
}
