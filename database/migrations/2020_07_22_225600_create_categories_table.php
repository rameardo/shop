<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique()->nullable(); // Category name
            $table->integer('parent_id')->unsigned()->nullable()->default(null); // is parent id or not?
            $table->foreign('parent_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('set null'); // when this delete, delete also parent category
            $table->text('icon')->nullable(); // Has an icon?
            $table->text('image')->nullable(); // Has an image?
            $table->boolean('visibility')->nullable(); // Category visibility or is active?
            $table->boolean('show_in_homepage')->nullable(); // want to show in homepage menu?
            $table->boolean('show_in_search')->nullable(); // want to show in header search dropdown?
            $table->boolean('show_category_image')->nullable(); // want to show category image in homepage menu?
            $table->integer('order')->default(1)->nullable(); // Category order
            $table->integer('homepage_order')->nullable(); // Category Homepage order
            $table->string('title')->nullable(); // Category meta data title
            $table->string('slug')->unique(); // Category url (belong to category name)
            $table->text('description')->nullable(); // Category meta data Description
            $table->timestamps(); // Creation & updating date and time.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
