<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable()->unique();
            $table->string('shortcut')->nullable()->unique();
            $table->string('code')->nullable()->unique();
            $table->text('icon_code')->nullable();
            $table->integer('position')->nullable()->unique();
            $table->boolean('status')->default(true)->nullable();
            $table->string('dirction')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
