@extends('front.layout.app')

@section('title', 'Home')

@section('content')
		
	<div id="homepage-3">
		<div class="container" style="margin: 5rem auto;">

<ul>
@foreach($categories as $category)
        <li>{{ $category->name }}
            @if(count( $category->subcategory) > 0 )
                <ul>
                @foreach($category->subcategory as $subcategory)
                    <li>{{ $subcategory->name }}</li>
                @endforeach 
                </ul>
            @endif
        </li>                   
@endforeach
</ul>
@php


@endphp

		</div>	
	</div>	

@stop