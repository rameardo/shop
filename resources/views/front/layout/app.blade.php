<!DOCTYPE html>
<html>
	<head>

		@include ('front.assets._meta')

		<title>Shop - @yield('title')</title>

		@include('front.assets._css')

		@yield('css')

		@yield('js_header')


	</head>
	<body>

		@include('front.partails.header')

		@yield('content')

		@include('front.partails.footer')

		@include('front.assets._js')

		@yield('js_footer')

	</body>
</html>