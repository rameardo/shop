        <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext" rel="stylesheet">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/fonts/Linearicons/Linearicons/Font/demo-files/demo.css">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/plugins/bootstrap4/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/plugins/owl-carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/plugins/slick/slick/slick.css">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/plugins/lightGallery-master/dist/css/lightgallery.min.css">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/plugins/jquery-ui/jquery-ui.min.css">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/plugins/select2/dist/css/select2.min.css">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/css/style.css">
        <link rel="stylesheet" href="{{ url('/') }}/cdn/static/css/market-place-1.css">

        