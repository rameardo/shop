        <script src="{{ url('/') }}/cdn/static/plugins/jquery-1.12.4.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/popper.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/owl-carousel/owl.carousel.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/bootstrap4/js/bootstrap.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/imagesloaded.pkgd.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/masonry.pkgd.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/isotope.pkgd.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/jquery.matchHeight-min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/slick/slick/slick.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/slick-animation.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/lightGallery-master/dist/js/lightgallery-all.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/sticky-sidebar/dist/sticky-sidebar.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/jquery.slimscroll.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/select2/dist/js/select2.full.min.js"></script>
        <script src="{{ url('/') }}/cdn/static/plugins/gmap3.min.js"></script>
        <!-- custom scripts-->
        <script src="{{ url('/') }}/cdn/static/js/main.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxflHHc5FlDVI-J71pO7hM1QJNW1dRp4U&amp;region=GB"></script>
        