@extends('front.layout.app')

@section('title', '404')

@section('css')

<style>
    .ps-page--404{min-height: 60vh;}
    .ps-page--404 .container {min-height: 70vh;}
    .ps-section__content img {
        width: 55%;
        margin-bottom: 5rem;
    }
</style>

@stop

@section('content')
    
    <div class="ps-page--404">
        <div class="container">
            <div class="ps-section__content"><img src="{{ url('/') }}/cdn/static/img/404.jpg" alt="">
                <h3>ohh! page not found</h3>
                <p>It seems we can't find what you're looking for. Perhaps searching can help or go back to<a href="{{url('/')}}"> Homepage</a></p>
                <form class="ps-form--widget-search" action="do_action" method="get">
                    <input class="form-control" type="text" placeholder="Search...">
                    <button><i class="icon-magnifier"></i></button>
                </form>
            </div>
        </div>
    </div>

@stop

