@extends('back.layout.app')

@section('title', 'Categories')

@section('css')
<style>
/*.btn-sm, .btn-group-sm > .btn{padding: 0 !important;}*/
.tb-col-action .btn:hover::before, .btn-trigger:before {background-color:unset !important;}
.remove .bg-light {background-color: #f6f9ff !important;}
.remove .modal-footer {padding: .25rem 1.5rem !important; border-color: #f6f9ff !important;}
</style>
@endsection

@section('content')

	<!-- content @s -->
    <div class="nk-content ">
    	<div class="container-fluid">
    		<div class="nk-content-inner">
    			<div class="nk-content-body">


                    @if (count($categories) > 0)
    				<div class="components-preview wide-md mx-auto">
                            <div class="nk-block-head">
                                
                                <div class="nk-block-between-md g-4">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title">Categories</h4>
                                        
                                    </div><!-- .nk-block-head-content -->

                                     @if (_can('create'))

                                        <div class="nk-block-head-content">
                                            <ul class="nk-block-tools gx-3">
                                                <li>
                                                	<a href="{{ route('admin.categories.new') }}" class="btn btn-white btn-light">
                                                		<span>Add Category</span> 
                                                		<em class="icon ni ni-plus-sm d-none d-sm-inline-block"></em>

                                                	</a>
                                                </li>
                                            </ul>
                                        </div><!-- .nk-block-head-content -->
                                    @endif
                                </div><!-- .nk-block-between -->
                            </div>


                                    <div class="nk-block nk-block-lg">



                                        <div class="card card-bordered card-preview">
                                            <table class="table table-ulogs">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th class="tb-tnx-id"><span class="overline-title">#</th>
                                                        <th class="tb-col-ip"><span class="overline-title">Category Name</span></th>
                                                        <th class="tb-col-time"><span class="overline-title">Parent Category</span></th>
                                                        <th class="tb-col-action"><span class="overline-title">&nbsp;</span></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach($categories as $category)

                                                    <tr>
                                                        <td class="tb-tnx-id">{{$category->id}}</td>
                                                        <td class="tb-col-ip">{{$category->name}}</td>

                                                        <td class="tb-col-time"> {{$category->parent_id != null ? $category->parent->name : '—'}}</td>



                                                        <td class="tb-col-action">
                                                            <div class="">
                                                                <ul class="nk-tb-actions gx-2">

                                                                    <li>
                                                                        <div class="drodown">
                                                                            <a href="#" class="btn btn-sm btn-icon btn-trigger dropdown-toggle" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                                <ul class="link-list-opt no-bdr">

                                                                                    <li><a href="{{url('/admin/categories')}}/{{$category->slug}}"><em class="icon ni ni-eye"></em><span>View Details</span></a></li>

                                                                                    <li class="divider"></li>

                                                                                    <li><a href="#"><em class="icon ni ni-edit-alt"></em><span>Edit</span></a></li>
                                                                                                                                   
                                                                                    <li>
                                                                                        <a href="#" class="delete" data-toggle="modal" id="delete" data-target="#remove" data-id="{{ $category->id }}">
                                                                                            <em class="icon ni ni-trash"></em>
                                                                                            <span>Remove</span>
                                                                                        </a>
                                                                                    </li>
                                               
                                               
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>                                                            
                                                        </td>


                                                        <!-- <td class="tb-col-action">
                                                        	<a href="#" class="link-cross mr-sm-n1"><em class="icon ni ni-cross"></em></a>
                                                        </td> -->
                                                    </tr>

                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div><!-- .card-preview -->
                                    </div><!-- nk-block -->

                                            <div class="d-flex justify-content-between align-items-center mt-3">

                                                <div>
                                                    {{ $categories->links() }}
                                                 



                                                </div> 
                                                
                                                <div>
                                                   Total {{$total}}
                                                </div>

                                            </div>                                    

    				</div>


                    @else

                    <div class="nk-content ">
                        <div class="nk-block nk-block-middle wide-md mx-auto">
                            <div class="nk-block-content nk-error-ld text-center">
                                <img class="nk-error-gfx" src="{{url('/')}}/cdn/back/images/gfx/error-504.svg" alt="">
                                <div class="wide-xs mx-auto">
                                    <h3 class="nk-error-title">WE ARE SORRY!</h3>
                                    <p class="nk-error-text">Currently, there are not categories available! 
                                    <br>
                                    <br>
                                    <a href="/admin/categories/new">Create new Category?</a></p>
            

                                </div>
                            </div>
                        </div><!-- .nk-block -->
                    </div>

                    @endif


    			</div>
    		</div>
    	</div>
    </div>
    <!-- content @e -->





    <!-- Remove Category -->
    <div class="modal zoom remove" tabindex="-1" id="remove">



            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <em class="icon ni ni-cross"></em>
                    </a>
                    <div class="modal-header">
                        <h5 class="modal-title">Are you sure?</h5>
                    </div>
                    <div class="modal-body">
                        <p>Do you really want to delete these records?</p>

                        <form action="/admin/categories/destroy" method="POST">
                        @csrf
                        
                        <input type="hidden" name="id" id="id" value="">

                    </div>

                    <div class="modal-footer bg-light justify-content-end">

                        <a href="#" class="btn btn-light" data-dismiss="modal">Cancel</a>

                        <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </div>

                </div>
            </div>

        
    </div>    

    @section('js_footer')

        <script>
            $('.delete').click(function(){
                var bt = $(this);
                var id = document.getElementById("id");
                id.value = bt.attr('data-id');
                console.log(bt.attr('data-id'));
            });         

        </script>

    @endsection


    @if (session('error'))

        {{dialog('error', 'Erro Happend!', session('error'), 'Please contact your <a href="#">System Administrator</a> or <a href="#">Support</a>.')}}

        @section('js_error_code')
            {{dialog_js('error')}}
        @endsection

    @endif


    @if (session('success'))

        {{dialog('success', 'Success!', session('success'))}}

        @section('js_success_code')
            {{dialog_js('success')}}
        @endsection
    @endif


@endsection


