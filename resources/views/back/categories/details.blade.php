@extends('back.layout.app')

@section('title', $category->name)

@section('content')



	<!-- content @s -->
    <div class="nk-content ">
    	<div class="container-fluid">
    		<div class="nk-content-inner">
    			<div class="nk-content-body">
    				<div class="components-preview wide-md mx-auto">
	                        

                                    <div class="nk-block">
                                        <div class="card card-bordered sp-plan">
                                            <div class="row no-gutters">
                                                <div class="col-md-8">
                                                    <div class="sp-plan-info card-inner">
                                                        <div class="row gx-0 gy-3">
                                                            <div class="col-xl-9 col-sm-8">
                                                                <div class="sp-plan-name">
                                                                    <h6 class="title">
                                                                    	<a href="http://{{Request::getHost()}}/c/{{$category->slug}}" target="_blank">{{$category->name}} 
                                                                    	</a>
                                                                    </h6>
                                                                    <p>Parent Category: <span class="text-base">{{$category->parent_id != null ? $category->parent->name : '—'}}</span></p>
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-3 col-sm-4">
                                                                <div class="sp-plan-opt">
                                                                    <div class="custom-control custom-switch">
                                                                        <input type="checkbox" class="custom-control-input" id="auto-plan-p1" {{$category->visibility == 1 ? 'checked' : ''}} disabled>
                                                                        <label class="custom-control-label text-soft" for="auto-plan-p1">{{$category->visibility == 1 ? 'Visibilil' : 'Invisibilil'}}</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- .sp-plan-info -->
                                                    <div class="sp-plan-desc card-inner">
                                                        <ul class="row gx-1">
                                                            <li class="col-6 col-lg-3">
                                                                <p><span class="text-soft">Created at</span> {{$category->created_at->toFormattedDateString()}}</p>
                                                            </li>
                                                            <li class="col-6 col-lg-3">
                                                                <p>
                                                                	<span class="text-soft">Active in Fronend</span>
                                                                	Yes
                                                                </p>
                                                            </li>
                                                            <li class="col-6 col-lg-3">
                                                                <p><span class="text-soft">Price</span> $599.00</p>
                                                            </li>
                                                            <li class="col-6 col-lg-3">
                                                                <p><span class="text-soft">Access</span> Unlimited</p>
                                                            </li>
                                                        </ul>
                                                    </div><!-- .sp-plan-desc -->
                                                </div><!-- .col -->
                                                <div class="col-md-4">
                                                    <div class="sp-plan-action card-inner">
                                                        <div class="sp-plan-btn">
                                                            <a href="http://{{Request::getHost()}}/c/{{$category->slug}}" class="btn btn-primary" data-toggle="modal" data-target="#subscription-change">
                                                            	<span>Edit</span>
                                                            	<em class="icon ni ni-arrow-long-right"></em>
                                                            </a>
                                                        </div>
                                                        <div class="sp-plan-note text-md-center">
                                                            <p>Last update <span>{{$category->updated_at->toFormattedDateString()}}</span></p>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                            </div><!-- .row -->
                                        </div><!-- .sp-plan -->

                                    </div><!-- .nk-block -->



                                    <div class="nk-block">
                                        <div class="card sp-plan">
                                            <div class="row no-gutters">

                                            	<div class="col-md-7 pr-2">

			                                        <div class="card card-bordered">
			                                            <div class="card-inner-group">
			                                                <div class="card-inner">
			                                                    <div class="between-center flex-wrap flex-md-nowrap g-3">
			                                                        <div class="nk-block-text">
			                                                            <h6>Title in Search Engines <span class="text-base">(Meta Data)</span></h6>
			                                                            <p class="text-soft">Shop | {{$category->title}}</p>
			                                                        </div>
			                                                    </div>
			                                                </div><!-- .nk-card-inner -->
			                                                <div class="card-inner">
			                                                    <div class="between-center flex-wrap flex-md-nowrap g-3">
			                                                        <div class="nk-block-text">
			                                                            <h6>Description in Search Engines <span class="text-base">(Meta Data)</span></h6>
			                                                            <p class="text-soft">{{$category->description}}</p>
			                                                        </div>
			                                                    </div>
			                                                </div><!-- .nk-card-inner -->
			                                                <div class="card-inner">
			                                                    <div class="between-center flex-wrap flex-md-nowrap g-3">
			                                                        <div class="nk-block-text">
			                                                            <p><a href="#">{{Request::getHost()}}/c/{{$category->slug}}</a></p>
			                                                        </div>
			                                                    </div>
			                                                </div><!-- .nk-card-inner -->
			                                                
			                                            </div>
						

			                                        </div>    
			                                                                                		
                                            	</div>

                                            	<div class="col-md-5">
			                                        <div class="card card-bordered card-preview">
			                                            <table class="table table-ulogs">
			                                                <thead class="thead-light">
			                                                    <tr>
			                                                        <th class="tb-col-os"><span class="overline-title">Property</span></th>
			                                                        <th class="tb-col-ip"><span class="overline-title"></span></th>
			                                                    </tr>
			                                                </thead>
			                                                <tbody>

			                                                    <tr>
			                                                        <td class="tb-col-os">Visibility</td>
			                                                        <td class="tb-col-ip"><span class="badge badge-dot badge-{{$category->visibility == 1 ? 'primary' : 'secondary'}}">{{$category->visibility == 1 ? 'Yes' : 'No'}}</span></td>
			                                                    </tr>
			       	                                                	
			                                                    <tr>
			                                                        <td class="tb-col-os">Show on Homepage</td>
			                                                        <td class="tb-col-ip"><span class="badge badge-dot badge-{{$category->show_on_homepage == 1 ? 'primary' : 'secondary'}}">{{$category->show_on_homepage == 1 ? 'Yes' : 'No'}}</span></td>
			                                                    </tr>
			       	                                                	
			                                                    <tr>
			                                                        <td class="tb-col-os">Show on Search-dropdown</td>
			                                                        <td class="tb-col-ip"><span class="badge badge-dot badge-{{$category->show_in_search == 1 ? 'primary' : 'secondary'}}">{{$category->show_in_search == 1 ? 'Yes' : 'No'}}</span></td>
			                                                    </tr>
			       
						       	                                                	
			                                                    <tr>
			                                                        <td class="tb-col-os">Show Category Image on the Navigation</td>
			                                                        <td class="tb-col-ip"><span class="badge badge-dot badge-{{$category->show_category_image == 1 ? 'primary' : 'secondary'}}">{{$category->show_category_image == 1 ? 'Yes' : 'No'}}</span></td>
			                                                    </tr>
			       
									       	                                                	
			                                                    <tr>
			                                                        <td class="tb-col-os">Show Category Image on the Navigation</td>
			                                                        <td class="tb-col-ip"><span class="badge badge-dot badge-primary">yes</span></td>
			                                                    </tr>
			       
						       
									       	                                                	
			                                                    <tr>
			                                                        <td class="tb-col-os">Show Category Image on the Navigation</td>
			                                                        <td class="tb-col-ip"><span class="badge badge-dot badge-primary">yes</span></td>
			                                                    </tr>
			       
			

			                                                </tbody>
			                                            </table>
			                                        </div><!-- .card-preview -->

                                            	</div>

                                            </div>
                                        </div>
									</div>



    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <!-- content @e -->











@endsection