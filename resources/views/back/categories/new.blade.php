@extends('back.layout.app')

@section('title', 'Add Category')

@section('css')

<style>
.form-img-b {
    width: 200px;
    height: 117px;
    background-size: 100%;
    background-repeat: no-repeat;
}    
</style>

@endsection

@section('content')

                <!-- content @s -->
                <div class="nk-content nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-body">

                            <form class="form-validate is-alter main-form" novalidate="novalidate" action="/admin/categories/create" method="POST" data-select2-id="6">

                                @csrf

    							<div class="nk-block-head" style="padding-bottom: 1.5rem;">
                                    <div class="nk-block-between-md g-2">
                                        <div class="nk-block-head-content">
                                            <h4 class="nk-block-title">New Category</h4>
                                            
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <ul class="nk-block-tools gx-3">
                                                <li>
                                                    <button type="submit" class="btn btn-primary">
                                                        <span>Create</span>
                                                        <em class="icon ni ni-arrow-long-right"></em>
                                                    </button>

                                                </li>
                                            </ul>
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div>

                                <div class="nk-block">
                                    <div class="card card-custom-s1 card-bordered">
                                        <div class="row no-gutters">
                                            <div class="col-lg-4">
                                                <div class="card-inner-group h-100">

                                                    <div class="card-inner">
                                                        <div class="row">

                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="fva-name">Category Name</label>
                                                                    <div class="form-control-wrap">
                                                                        <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{old('name')}}" id="fva-name" name="name" placeholder="Category Name" autocomplete="off" required>
                                                                        @error('name')
                                                                        <div class="invalid-feedback">
                                                                            {{$message}}
                                                                        </div>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 mt-2">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="fv-topics">Parent Category</label>
                                                                    <div class="form-control-wrap ">
                                                                        <select class="form-control form-select" id="fv-topics" name="parent_id" data-placeholder="None">
                                                                            <option label="empty" value=""></option>

                                                                            @foreach($categories as $category)
                                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                                            @endforeach

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="card-inner">
    													<div class="nk-block-content-head">
                                                            <h6 class="text-muted">Category properties</h6>
                                                        </div>

    													<div class="d-block custom-control custom-control-sm custom-switch mb-1">
    													    <input type="checkbox" class="custom-control-input" name="visibility" id="visibility" value="1" checked>
    													    <label class="form-label custom-control-label" for="visibility">Visibility</label>
    													</div>


    													<div class="d-block custom-control custom-control-sm custom-switch mb-1">
    													    <input type="checkbox" class="custom-control-input" name="show_in_homepage" id="homepage">
    													    <label class="form-label custom-control-label" for="homepage">Show on Homepage</label>
    													</div>



                                                        <div class="d-block custom-control custom-control-sm custom-switch mb-1">
                                                            <input type="checkbox" class="custom-control-input" name="show_icon" id="show_icon">
                                                            <label class="form-label custom-control-label" for="show_icon">Show icon on Sidebar</label>
                                                        </div>


                                                        <div class="d-block custom-control custom-control-sm custom-switch mb-1">
                                                            <input type="checkbox" class="custom-control-input" name="show_in_search" id="show_on_search">
                                                            <label class="form-label custom-control-label" for="show_on_search">Show on Search-dropdown</label>
                                                        </div>



    													<div class="d-block custom-control custom-control-sm custom-switch mb-1">
    													    <input type="checkbox" class="custom-control-input" name="show_category_image" id="navigate" checked>
    													    <label class="form-label custom-control-label" for="navigate">Show Category Image on the Navigation</label>
    													</div>

                                                        <div class="row">

                                                            <div class="col-md-12 mt-1">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="fva-order">Order</label>
                                                                    <div class="form-control-wrap">
                                                                        <input type="number" class="form-control @error('order') is-invalid @enderror" min="0" id="fva-order" name="order" placeholder="Order" value="{{old('name')}}" required>
                                                                        @error('order')
                                                                        <div class="invalid-feedback">
                                                                            {{$message}}
                                                                        </div>
                                                                        @enderror                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        	
                                                            <div class="col-md-12 mt-3">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="fva-homepage">Homepage Order</label>
                                                                    <div class="form-control-wrap">
                                                                         <input type="number" class="form-control @error('homepage_order') is-invalid @enderror" min="0" id="fva-homepage" name="homepage_order" placeholder="Order" value="{{old('homepage_order')}}" required>
                                                                        @error('homepage_order')
                                                                        <div class="invalid-feedback">
                                                                            {{$message}}
                                                                        </div>
                                                                        @enderror    
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>													

                                                    </div>

                                                </div>
                                            </div><!-- .col -->
                                            <div class="col-lg-8">
                                                <div class="card-inner card-inner-lg h-100">
                                                    <div class="align-center flex-wrap flex-md-nowrap g-3 h-100">

                                                        <div class="nk-block-content">
                                                            <div class="nk-block-content-head">
                                                                <h4>SEO Information</h4>
                                                                <span class="sub-text sub-text-sm text-soft">
                                                                	Please type information related to the name of the item so that it is well archived in the search engines.
                                                                </span>
                                                            </div>

                                                            <p>
                                                            	Adhere to the following rules when creating a new Category:
                                                            </p>

                                                            <div class="d-flex justify-content-between">
        														<ul class="list list-sm list-checked">
                                                                    <li>Name that matches the content of the Category <span>(Classical English)</span></li>
                                                                    <li>A similar or identical link to the name of the Category<span> (Slug input)</span></li>
                                                                    <li>Accurate description of the Category</li>
                                                                </ul>
                                                                <div class="form-img-b" style="background-image: url({{asset('cdn/back/images/gfx/seo.png')}});"></div>
                                                            </div>

    	                                                    <div class="row g-gs">
    	                                                        <div class="col-md-6">
    	                                                            <div class="form-group">
    	                                                                <label class="form-label" for="fva-title">Title (Meta Tag)</label>
    	                                                                <div class="form-control-wrap">
    	                                                                    <input type="text" class="form-control" id="fva-title" name="title" placeholder="Title" required>
    	                                                                </div>
    	                                                            </div>
    	                                                        </div>


    	                                                        <div class="col-md-6">
    	                                                            <div class="form-group">
    	                                                                <label class="form-label" for="fva-slug">
    	                                                                	Slug
    	                                                                	
    	                                                                </label>
    	                                                                <div class="input-group form-control-wrap">
    																	  <div class="input-group-prepend">
    																	    <span class="input-group-text" id="basic-addon3">{{Request::getHost()}}/c/</span>
    																	  </div>	                                                                	
    	                                                                    <input type="text" class="form-control" id="fva-slug" name="slug" placeholder="Slug">
    	                                                                </div>
    	                                                                <span class="sub-text sub-text-sm text-soft mt-1">(If you leave it blank, it will be generated automatically.)</span>
    	                                                            </div>
    	                                                        </div>

    	                                                        <div class="col-md-12">
    	                                                            <div class="form-group">
    	                                                                <label class="form-label" for="fva-message">Description (Meta Tag)</label>
    	                                                                <div class="form-control-wrap">
    	                                                                    <textarea class="form-control form-control-sm" id="fva-message" name="description" placeholder="Description" required></textarea>
    	                                                                </div>
    	                                                            </div>
    	                                                        </div>

    	                    
    	                                                    </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- .col -->
                                        </div><!-- .row -->
                                    </div><!-- .card -->
                                </div><!-- .nk-block -->

                            </form>


                        </div>
                    </div>
                </div>
                <!-- content @e -->



@endsection

@section('js_footer')
<script>
$(function(){
    $('#fva-name').focus(); 
});
</script>
@endsection