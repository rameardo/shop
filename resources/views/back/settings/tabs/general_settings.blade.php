{{--                             <ul class="nk-nav nav nav-tabs">

                                <li class="nav-item {{ \Route::current()->getName() ==  'admin.general_settings' ? 'active' : '' }}">
                                    <a class="nav-link" href="{{route('admin.general_settings')}}">Geneeral Settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Header Message</a>
                                </li>                                
                                <li class="nav-item {{ \Route::current()->getName() ==  'admin.general_settings.contact_settings' ? 'active' : '' }}">
                                    <a class="nav-link" href="{{route('admin.general_settings.contact_settings')}}">Contact Settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Social Media Settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Cookies Settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Google reCAPTCHA</a>
                                </li>
                                
                            </ul> 
--}}
                            <div style="min-height: 73vh" class="card-aside card-aside-left user-aside toggle-slide toggle-slide-left toggle-break-lg" data-content="userAside" data-toggle-screen="lg" data-toggle-overlay="true">
                                <div class="card-inner-group">


                                    <div class="card-inner p-0">
                                        <ul class="link-list-menu">

                                            <li>
                                                <a class="{{ \Route::current()->getName() ==  'admin.general_settings' ? 'active' : '' }}" href="{{route('admin.general_settings')}}">
                                                    <em class="icon ni ni-slack-hash"></em>
                                                    <span>Geneeral Settings</span>
                                                </a>
                                            </li>
                                            
                                            <li>
                                                <a class="" href="#">
                                                    <em class="icon ni ni-slack-hash"></em>
                                                    <span>Header Message</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a class="{{ \Route::current()->getName() ==  'admin.general_settings.contact_settings' ? 'active' : '' }}" href="{{route('admin.general_settings.contact_settings')}}">
                                                    <em class="icon ni ni-slack-hash"></em>
                                                    <span>Contact Settings</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a class="" href="#">
                                                    <em class="icon ni ni-slack-hash"></em>
                                                    <span>Social Media Settings</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a class="" href="#">
                                                    <em class="icon ni ni-slack-hash"></em>
                                                    <span>Cookies Settings</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a class="" href="#">
                                                    <em class="icon ni ni-slack-hash"></em>
                                                    <span>Google reCAPTCHA</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div><!-- .card-inner -->                                                    

                                </div><!-- .card-inner-group -->
                            </div><!-- card-aside -->


