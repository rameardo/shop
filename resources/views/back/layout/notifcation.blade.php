
                                    {{-- Start Notifcation --}}
                                    <li class="dropdown backend-notifcations notification-dropdown mr-n1">
                                        <a id="notifcationClick" href="#" class="dropdown-toggle nk-quick-nav-icon" data-toggle="dropdown">
                                            <div class="icon-status">
                                                {{-- {!! $backend_notifcations_counter > 0 ? '<span class="bg-danger notifcation-count">' . $backend_notifcations_counter . '</span>' : '' !!} --}}

                                                @if ($backend_notifcations_counter > 0 && $backend_notifcations_counter < 9)

                                                    <span class="bg-danger notifcation-count"> {{$backend_notifcations_counter}} </span>

                                                @elseif ($backend_notifcations_counter > 9)
                                                    <span class="bg-danger notifcation-count"> +9 </span>

                                                @endif

                                                <em class="icon ni ni-bell"></em>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right ">

                                            <div class="dropdown-head">
                                                <span class="sub-title nk-dropdown-title">Notifications</span>
                                                <a href="#">Mark All as Read</a>
                                            </div>

                                            <div class="dropdown-body">
                                                <div class="nk-notification">

                                                    @if (count($backend_notifcations) > 0)
                                                        @foreach($backend_notifcations as $notifcation)

                                                            <div class="nk-notification-item dropdown-inner">

                                                                @if ($notifcation->icon !== null)
                                                                    <div class="nk-notification-icon">
                                                                        <em class="icon icon-circle {{$notifcation->icon}}"></em>
                                                                    </div>
                                                                @else
                                                                    <div class="nk-notification-icon">
                                                                        <em class="icon icon-circle bg-indigo-dim ni ni-info-i"></em>
                                                                    </div>                                                                
                                                                @endif
                                                                
                                                                <a href="{{$notifcation->link !== null && strlen($notifcation->link) != 0 ? $notifcation->link : '#' . \Str::slug($notifcation->title, '-') }}" class="nk-notification-content">

                                                                    <div class="nk-notification-text title fw-medium">  
                                                                        {{$notifcation->title}}
                                                                    </div>

                                                                    @if(strlen($notifcation->descriptions) > 0)
                                                                        <div class="nk-notification-text descriptions text-muted">
                                                                            {{$notifcation->descriptions}}
                                                                        </div>
                                                                    @endif

                                                                    @if ($notifcation->created_at !== null)
                                                                        <div class="nk-notification-time {{strlen($notifcation->descriptions) > 0 ? 'mt-1' : ''}}">
                                                                            {{$notifcation->created_at->diffForHumans(
                                                                                ['options' => \Carbon\Carbon::ONE_DAY_WORDS | \Carbon\Carbon::JUST_NOW | \Carbon\Carbon::NO_ZERO_DIFF]
                                                                            )}}
                                                                        </div>
                                                                    @endif

                                                                </a>

                                                            </div>

                                                        @endforeach

                                                    @else

                                                        {{-- Start No Notifcations --}}
                                                        <div class="no-notifcations">
                                                            <img src="{{asset('cdn/back/icons/notifcations.svg')}}">
                                                            <p class="text-muted mt-1">
                                                                There are no Notifcations
                                                            </p>
                                                        </div>
                                                        {{-- End No Notifcations --}}                                                    

                                                    @endif

                                                </div>
                                            </div><!-- .nk-dropdown-body -->

                                            @if (count($backend_notifcations) > 0)
                                                <div class="dropdown-foot center">
                                                    <a href="#">View All</a>
                                                </div>
                                            @endif

                                        </div>
                                    </li>
                                    {{-- End Notifcation --}}

                                    