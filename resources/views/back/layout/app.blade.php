<!DOCTYPE html>
<html>
	<head>

		@include ('back.assets._meta')

		<title>Admin - @yield('title')</title>

		@include('back.assets._css')

		@yield('css')

		@yield('js_header')
		<script>
			var current_url = '{{\Request::fullUrl() }}';
			var website_url = '{{websiteUrl()}}';
			var ajax = '/cdn/back/ajax/ajax.js';
		</script>


	</head>
	<body class="nk-body npc-crypto has-sidebar has-sidebar-fat ui-clean">

		<div class="nk-app-root">
			<!-- main @s -->
			<div class="nk-main ">

	        	@include('back.partails.sidebar')



	        	<!-- wrap @s -->
	        	<div class="nk-wrap" id="main-content">


	        		
					@include('back.partails.header')



					@yield('content')



					@include('back.partails.footer')

				</div>
				<!-- wrap @e -->
			</div>
			<!-- main @e -->
		</div>
		<!-- app-root @e -->

		@include('back.assets._js')

		@yield('js_footer')
		@yield('js_error_code')
		@yield('js_success_code')
		

	</body>
</html>