@extends('back.layout.app')

@section('title', 'Dashboard')

@section('js_footer')
<script src="{{asset('cdn/back/ajax/dashboard/dashboard.js')}}"></script>
@endsection

@section('content')

                <!-- content @s -->
                <div class="nk-content nk-content-fluid">
               
                    <div class="container-xl wide-lg">
                        <div class="nk-content-body">


                            <div class="nk-block nk-block-lg">

                                <div class="row g-gs">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="card card-bordered text-light is-dark h-100">
                                            <div class="card-inner">
                                                <div class="nk-wg7">
                                                    <div class="nk-wg7-stats">
                                                        <div class="nk-wg7-title">Available balance in USD</div>
                                                        <div class="number-lg amount">179,850.950</div>
                                                    </div>
                                                    <div class="nk-wg7-stats-group">
                                                        <div class="nk-wg7-stats w-50">
                                                            <div class="nk-wg7-title">Wallets</div>
                                                            <div class="number-lg">5</div>
                                                        </div>
                                                        <div class="nk-wg7-stats w-50">
                                                            <div class="nk-wg7-title">Transactions</div>
                                                            <div class="number">34,405</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-wg7-foot">
                                                        <span class="nk-wg7-note">Last activity at <span>19 Nov, 2019</span></span>
                                                    </div>
                                                </div><!-- .nk-wg7 -->
                                            </div><!-- .card-inner -->
                                        </div><!-- .card -->
                                    </div>
                                    <div class="col-md-6 col-lg-4">
                                        <div class="card card-bordered clickCount">
                                            <div class="nk-wgw">
                                                <div class="nk-wgw-inner">
                                                    <a class="nk-wgw-name" href="#">
                                                        <div class="nk-wgw-icon is-default">
                                                            <em class="icon ni ni-slack-hash"></em>
                                                        </div>
                                                        <h5 class="nk-wgw-title title">Total Orders</h5>
                                                    </a>
                                                    <div class="nk-wgw-balance">
                                                        <div class="amount" id="translations">{{number_format(ajaxCount('translations'))}}</div>
                                                        <div class="amount-sm">12,495.90<span class="currency currency-usd">Today</span></div>
                                                    </div>
                                                </div>
   
                                                <div class="nk-wgw-more">
                                                    <a id="click_me" class="btn btn-icon btn-trigger" ><em class="icon ni ni-reload-alt"></em></a>
                                                </div>
                                            </div>
                                        </div><!-- .card -->
                                    </div><!-- .col -->

                                </div><!-- .row -->
                            </div>

                            <div class="nk-block">
                                <div class="row gy-gs">
                                    <div class="col-lg-5 col-xl-4">
                                        <div class="nk-block">
                                            <div class="nk-block-head-xs">
                                                <div class="nk-block-head-content">
                                                    <h5 class="nk-block-title title">Overview</h5>

                                                </div>
                                            </div><!-- .nk-block-head -->
                                            <div class="nk-block">
                                                <div class="card card-bordered text-light is-dark h-100">
                                                    <div class="card-inner">
                                                        <div class="nk-wg7">
                                                            <div class="nk-wg7-stats">
                                                                <div class="nk-wg7-title">Available balance in USD</div>
                                                                <div class="number-lg amount">179,850.950</div>
                                                            </div>
                                                            <div class="nk-wg7-stats-group">
                                                                <div class="nk-wg7-stats w-50">
                                                                    <div class="nk-wg7-title">Wallets</div>
                                                                    <div class="number-lg">5</div>
                                                                </div>
                                                                <div class="nk-wg7-stats w-50">
                                                                    <div class="nk-wg7-title">Transactions</div>
                                                                    <div class="number">34,405</div>
                                                                </div>
                                                            </div>
                                                            <div class="nk-wg7-foot">
                                                                <span class="nk-wg7-note">Last activity at <span>19 Nov, 2019</span></span>
                                                            </div>
                                                        </div><!-- .nk-wg7 -->
                                                    </div><!-- .card-inner -->
                                                </div><!-- .card -->
                                            </div><!-- .nk-block -->
                                        </div><!-- .nk-block -->
                                    </div><!-- .col -->
                                    <div class="col-lg-7 col-xl-8">
                                        <div class="nk-block">
                                            <div class="nk-block-head-xs">
                                                <div class="nk-block-between-md g-2">
                                                    <div class="nk-block-head-content">
                                                        <h5 class="nk-block-title title">Basic Overview</h5>
                                                    </div>
                                                    <div class="nk-block-head-content">
                                                        <a href="html/crypto/wallets.html" class="link link-primary">See All</a>
                                                    </div>
                                                </div>
                                            </div><!-- .nk-block-head -->
                                            <div class="row g-2">
                                                <div class="col-sm-4">
                                                    <div class="card bg-light">
                                                        <div class="nk-wgw sm">
                                                            <a class="nk-wgw-inner" href="#">
                                                                <div class="nk-wgw-name">
                                                                    <div class="nk-wgw-icon">
                                                                       <em class="icon ni ni-slack-hash"></em>
                                                                    </div>
                                                                    <h5 class="nk-wgw-title title">Total Orders</h5>
                                                                </div>
                                                                <div class="nk-wgw-balance">
                                                                    <div class="amount">4.434953<span class="currency currency-nio">Orders</span></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                                <div class="col-sm-4">
                                                    <div class="card bg-light">
                                                        <div class="nk-wgw sm">
                                                            <a class="nk-wgw-inner" href="html/crypto/wallet-bitcoin.html">
                                                                <div class="nk-wgw-name">
                                                                    <div class="nk-wgw-icon">
                                                                        <em class="icon ni ni-bag-fill"></em>
                                                                    </div>
                                                                    <h5 class="nk-wgw-title title">Total Products</h5>
                                                                </div>
                                                                <div class="nk-wgw-balance">
                                                                    <div class="amount">4.434953<span class="currency currency-btc">Products</span></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                                <div class="col-sm-4">
                                                    <div class="card bg-light">
                                                        <div class="nk-wgw sm">
                                                            <a class="nk-wgw-inner" href="html/crypto/wallet-bitcoin.html">
                                                                <div class="nk-wgw-name">
                                                                    <div class="nk-wgw-icon">
                                                                        <em class="icon ni ni-user-fill"></em>
                                                                    </div>
                                                                    <h5 class="nk-wgw-title title">Total Users</h5>
                                                                </div>
                                                                <div class="nk-wgw-balance">
                                                                    <div class="amount">0.000560<span class="currency currency-eth">ETH</span></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                            </div><!-- .row -->
                                        </div><!-- .nk-block -->
                                        <div class="nk-block nk-block-md">
                                            <div class="nk-block-head-xs">
                                                <div class="nk-block-between-md g-2">
                                                    <div class="nk-block-head-content">
                                                        <h6 class="nk-block-title title">Finance Overview </h6>
                                                    </div>
                                                    <div class="nk-block-head-content">
                                                        <a href="html/crypto/wallets.html" class="link link-primary">See All</a>
                                                    </div>
                                                </div>
                                            </div><!-- .nk-block-head -->
                                            <div class="row g-2">
                                                <div class="col-sm-4">
                                                    <div class="card bg-light">
                                                        <div class="nk-wgw sm">
                                                            <a class="nk-wgw-inner" href="html/crypto/wallet-bitcoin.html">
                                                                <div class="nk-wgw-name">
                                                                    <div class="nk-wgw-icon">
                                                                        <em class="icon ni ni-sign-mxn"></em>
                                                                    </div>
                                                                    <h5 class="nk-wgw-title title">Earnings</h5>
                                                                </div>
                                                                <div class="nk-wgw-balance">
                                                                    <div class="amount">4.434953<span class="currency currency-nio">NIO</span></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                                <div class="col-sm-4">
                                                    <div class="card bg-light">
                                                        <div class="nk-wgw sm">
                                                            <a class="nk-wgw-inner" href="html/crypto/wallet-bitcoin.html">
                                                                <div class="nk-wgw-name">
                                                                    <div class="nk-wgw-icon">
                                                                        <em class="icon ni ni-paypal-alt"></em>
                                                                    </div>
                                                                    <h5 class="nk-wgw-title title">Payouts</h5>
                                                                </div>
                                                                <div class="nk-wgw-balance">
                                                                    <div class="amount">4.434953<span class="currency currency-btc">BTC</span></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                                <div class="col-sm-4">
                                                    <div class="card bg-light">
                                                        <div class="nk-wgw sm">
                                                            <a class="nk-wgw-inner" href="html/crypto/wallet-bitcoin.html">
                                                                <div class="nk-wgw-name">
                                                                    <div class="nk-wgw-icon">
                                                                        <em class="icon ni ni-cc-alt"></em>
                                                                    </div>
                                                                    <h5 class="nk-wgw-title title">Transactions</h5>
                                                                </div>
                                                                <div class="nk-wgw-balance">
                                                                    <div class="amount">0.000560<span class="currency currency-eth">ETH</span></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                            </div><!-- .row -->
                                        </div> <!-- .nk-block -->
                                    </div><!-- .col -->
                                </div><!-- .row -->
                            </div><!-- .nk-block -->

                        </div>
                    </div>
                </div>
                <!-- content @e -->
        

@stop

