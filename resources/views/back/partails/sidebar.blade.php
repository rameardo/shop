            <!-- sidebar @s --> 
            <div class="nk-sidebar nk-sidebar-fat nk-sidebar-fixed" data-content="sidebarMenu">
                <div class="nk-sidebar-element nk-sidebar-head">
                    <div class="nk-sidebar-brand">
                        <a href="html/crypto/index.html" class="logo-link nk-sidebar-logo">
                            <img class="logo-light logo-img" src="{{ url('/') }}/cdn/back/images/logo.png" srcset="{{ url('/') }}/cdn/back/images/logo2x.png 2x" alt="logo">
                            <img class="logo-dark logo-img" src="{{ url('/') }}/cdn/back/images/logo-dark.png" srcset="{{ url('/') }}/cdn/back/images/logo-dark2x.png 2x" alt="logo-dark">
                            <span class="nio-version">Crypto</span>
                        </a>
                    </div>
                    <div class="nk-menu-trigger mr-n2">
                        <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
                    </div>
                </div><!-- .nk-sidebar-element -->
                <div class="nk-sidebar-element">
                    <div class="nk-sidebar-body" data-simplebar>
                        <div class="nk-sidebar-content">

                            <div class="nk-sidebar-menu">
                                <!-- Menu -->
                                <ul class="nk-menu">
                                    <li class="nk-menu-heading">
                                        <h6 class="overline-title">{{_trans('dashboard')}}</h6>
                                    </li>
                                    
                                    <li class="nk-menu-item {{ \Route::current()->getName() ==  'admin' ? 'active current-page' : '' }}">
                                        <a href="{{ route('admin') }}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-layer-fill"></em></span>
                                            <span class="nk-menu-text">{{_trans('dashboard')}}</span>
                                        </a>
                                    </li>


                                    <li class="nk-menu-item has-sub ">
                                        <a href="#" class="nk-menu-link nk-menu-toggle">
                                            <span class="nk-menu-icon"><em class="icon ni ni-folder-fill"></em></span>
                                            <span class="nk-menu-text">Content</span>
                                        </a>
                                        <ul class="nk-menu-sub">
                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">General Settings</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="nk-menu-item has-sub ">
                                        <a href="#" class="nk-menu-link nk-menu-toggle">
                                            <span class="nk-menu-icon"><em class="icon ni ni-folder-fill"></em></span>
                                            <span class="nk-menu-text">Blog</span>
                                        </a>
                                        <ul class="nk-menu-sub">
                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">General Settings</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="nk-menu-item ">
                                        <a href="#" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-files-fill"></em></span>
                                            <span class="nk-menu-text">Pages</span>
                                        </a>
                                    </li>


                                    <li class="nk-menu-item {{ \Route::current()->getName() ==  'admin.categories' ? 'active current-page' : '' }}">
                                        <a href="{{ route('admin.categories') }}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-folder"></em></span>
                                            <span class="nk-menu-text">{{_trans('categories')}}</span>
                                        </a>
                                    </li>




                                    <li class="nk-menu-heading">
                                        <h6 class="overline-title">{{_trans('settings')}}</h6>
                                    </li>


                                    <li class="nk-menu-item has-sub {{ str_contains(Route::currentRouteName(), 'admin.general_settings') ? 'active current-page' : '' }}">
                                        <a href="#" class="nk-menu-link nk-menu-toggle">
                                            <span class="nk-menu-icon"><em class="icon ni ni-sort-line"></em></span>
                                            <span class="nk-menu-text">Settings</span>
                                        </a>
                                        <ul class="nk-menu-sub">

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Preferences</span>
                                                </a>
                                            </li>

                                            <div class="space-between-list"></div>

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Payment Settings</span>
                                                </a>
                                            </li>

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Currency Settings</span>
                                                </a>
                                            </li>
                                            
                                            <div class="space-between-list"></div>

                                            <li class="nk-menu-item {{ str_contains(Route::currentRouteName(), 'admin.general_settings') ? 'active current-page' : '' }}">
                                                <a href="{{ route('admin.general_settings') }}" class="nk-menu-link">
                                                    <span class="nk-menu-text">General Settings</span>
                                                </a>
                                            </li>


      
                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Visual Settings</span>
                                                </a>
                                            </li>

                                            <div class="space-between-list"></div>
                                 

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">System Settings</span>
                                                </a>
                                            </li>

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">E-Mail Settings</span>
                                                </a>
                                            </li>

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Route Settings</span>
                                                </a>
                                            </li>

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Storage Settings</span>
                                                </a>
                                            </li>

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Cache System</span>
                                                </a>
                                            </li>

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">SEO Tools</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>


                                    <li class="nk-menu-item ">
                                        <a href="#" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-color-palette-fill"></em></span>
                                            <span class="nk-menu-text">Template</span>
                                        </a>
                                    </li>


                                    <li class="nk-menu-item has-sub ">
                                        <a href="#" class="nk-menu-link nk-menu-toggle">
                                            <span class="nk-menu-icon"><em class="icon ni ni-panel-fill"></em></span>
                                            <span class="nk-menu-text">Localization</span>
                                        </a>
                                        <ul class="nk-menu-sub">
                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Languages</span>
                                                </a>
                                            </li>

                                            <div class="space-between-list"></div>

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Countries</span>
                                                </a>                                               
                                            </li>

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">States</span>
                                                </a>                                               
                                            </li>

                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Cities</span>
                                                </a>                                               
                                            </li>

                                        </ul>
                                    </li>


                                    <li class="nk-menu-item has-sub ">
                                        <a href="#" class="nk-menu-link nk-menu-toggle">
                                            <span class="nk-menu-icon"><em class="icon ni ni-cc-visa"></em></span>
                                            <span class="nk-menu-text">Payments</span>
                                        </a>
                                        <ul class="nk-menu-sub">
                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Payments Settings</span>
                                                </a>
                                            </li>
                                            <li class="nk-menu-item ">
                                                <a href="#" class="nk-menu-link">
                                                    <span class="nk-menu-text">Currency Settings</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>


                                </ul><!-- .nk-menu -->
                            </div><!-- .nk-sidebar-menu -->
                        

                            <div class="nk-sidebar-footer">
                                <ul class="nk-menu nk-menu-footer">
                                    <li class="nk-menu-item">
                                        <a href="#" class="nk-menu-link fw-medium" data-original-title="" title="">
                                            <span class="nk-menu-icon"><em class="icon text-danger ni ni-shield-alert-fill"></em></span>
                                            <span class="nk-menu-text text-danger">Issue found?</span>
                                        </a>
                                    </li>


                                    @if (test_database_connection() === true)

                                        <li class="nk-menu-item ml-auto">
                                            <div class="dropup">
                                                <a href="#" class="nk-menu-link {{count($h_languages) > 0 ? 'dropdown-indicator has-indicator' : ''}} " data-toggle="dropdown" data-offset="0,10" data-original-title="" title="">
                                                    <span class="nk-menu-icon"><em class="icon ni ni-globe"></em></span>
                                                    <span class="nk-menu-text">{{current_lang('name')}}</span>
                                                </a>
                                                @if (count($h_languages) > 0)
                                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                                    <ul class="language-list">

                                                        @foreach($h_languages as $language)
                                                            <li>
                                                                <a href="{{url('/lang/' . $language->shortcut)}}" class="language-item">
                                                                    <img src="{{asset('/cdn/back/images/flags/english.png')}}" alt="" class="language-flag">
                                                                    <span class="language-name">{{$language->name}}</span>
                                                                </a>
                                                            </li>

                                                        @endforeach

                                                    </ul>
                                                </div>
                                                @endif
                                            </div>
                                        </li>
                                    
                                    @endif
                                </ul><!-- .nk-footer-menu -->
                            </div>                        

                        </div><!-- .nk-sidebar-contnet -->
                    </div><!-- .nk-sidebar-body -->
                </div><!-- .nk-sidebar-element -->
            </div>
            <!-- sidebar @e -->
