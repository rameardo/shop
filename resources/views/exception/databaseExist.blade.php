<!DOCTYPE html>
<html>
	<head>

		<title>{{thisApp('name')}} Handler | Database not exist</title>

		@include('back.assets._css')



	</head>
	<body class="nk-body bg-white npc-general pg-error">
	    <div class="nk-app-root">
	        <!-- main @s -->
	        <div class="nk-main ">
	            <!-- wrap @s -->
	            <div class="nk-wrap justify-center">
	                <!-- content @s -->
	                <div class="nk-content ">
	                    <div class="nk-block nk-block-middle wide-md mx-auto">
	                        <div class="nk-block-content nk-error-ld text-center">
	                            
	                            <div class="wide-xs mx-auto">
	                                <h3 class="nk-error-title">Oops! Database Error!</h3>
	                                <p class="nk-error-text">
	                                	It seems that <strong>{{env('DB_DATABASE')}}</strong> Database not exist.
	                                </p>
	                                
	                            </div>
	                        </div>
	                    </div><!-- .nk-block -->
	                </div>
	                <!-- wrap @e -->
	            </div>
	            <!-- content @e -->
	        </div>
	        <!-- main @e -->
	    </div>

		@include('back.assets._js')


	</body>
</html>
