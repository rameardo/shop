<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\Backend\AppSetting as Setting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;    
use View;


use App\Model\Pub\Notifcation;
use App\Model\Pub\Language;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // view()->addNamespace('installer', app_path('app/installer'));
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (test_database_connection() === true):
            // Global Variables
            $backend_notifcations = 
                                    Notifcation::where('where', 0)
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

            $backend_notifcations_counter = Notifcation::where('where', 0)
                       ->where('status', 1)
                       ->count();    
                       
            $h_languages = Language::where('name', '!=', current_lang('name'))->get();

            View::share('backend_notifcations', $backend_notifcations);
            View::share('backend_notifcations_counter', $backend_notifcations_counter);
            View::share('h_languages', $h_languages);

        endif;





        view()->addNamespace('thisApp', base_path(thisApp('view')));

        Schema::defaultStringLength(191);



        app()->singleton('lang', function(){
            $default_lang = Setting::where('name', '=', 'Language')->first();
            if (auth()->user())
            {
                if (empty(auth()->user()->lang))
                {
                    return $default_lang->value;
                }
                else
                {
                    return auth()->user()->lang;
                }
            }
            else
            {
                if (session()->has('lang'))
                {
                    return session()->get('lang');
                }
                else
                {
                    return $default_lang->value;
                }
            }
        });



       
        // dd($exception->getMessage());


        
    }
}
