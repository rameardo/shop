<?php

namespace App;

use App\Permissions\HasPermissionsTrait;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{


    
    // public function roles()
    // {
    //     return $this->belongsToMany('App\Model\Backend\Role', 'user_roles', 'user_id', 'role_id');
    // }

    // public function permissions()
    // {
    //     // return $this->belongsToMany('App\Model\Backend\Permission', 'user_id');
    //     return $this->hasOne('App\Model\Backend\Permission', 'user_id');
    // }


    // public function permissions()
    // {
    //     // return $this->belongsToMany('App\Model\Backend\Permission', 'user_id');
    //     return $this->belongsToMany('App\Model\Backend\Role', 'user_roles');
    // }


    // public function hasAnyRole($roles)
    // {

    //     if (is_array($roles)):

    //         foreach($roles as $role):

    //             if ($this->hasRole($role)):
    //                 return true;

    //             endif;

    //         endforeach;

    //     else:

    //         if ($this->hasRole($roles)):
    //             return true;

    //         endif;

    //     endif;

    // }

    // public function hasRole($role)
    // {

    //     if ($this->roles()->where('name', $role)->first()):

    //         return true;

    //     endif;

    //     return false;


    // }



    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];






}
