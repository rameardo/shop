<?php

namespace App\Model\Pub;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $fillable = [
        'id', 'lang_shortcut', 'key', 'value'
    ];
    
    public function lang()
    {
        return $this->belongsTo('App\Model\Pub\Language', 'shortcut');
    }
}
