<?php

namespace App\Model\Pub;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $fillable = ['name','parent_id'];

    public function subcategory()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent()
    {
    	return $this->belongsTo(Category::class, 'parent_id');
    }


}
