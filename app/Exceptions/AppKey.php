<?php

namespace App\Exceptions;

use Exception;

class AppKey extends Exception
{
	public function report()
	{
		\Log::info('Error Application key does not match.');
	}

	public function render($request)
	{
		// abort(403, 'sda');
		// return view('exception.queryDatabase');
		// return response()->view('exception.queryDatabase', compact('request'));
		// return response()->view('exception.queryDatabase', compact('exception'));
	}
}
