<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class PublicMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $database_connection = test_database_connection();
        $database_exists = check_database_exist();
        try {

            if ($database_exists === true && $database_connection === true):

                $hasRun = DB::table('migrations')->get();
                if ($hasRun == '[]' || !$hasRun):
                    return response()->view('thisApp::exception.migration', compact('exception'));
                else:
                    return $next($request);
                endif; 
            else:
                return $next($request);
            endif;
            
            
        } catch (Exception $e) {
            dd('error');
        }
 

    }
}
