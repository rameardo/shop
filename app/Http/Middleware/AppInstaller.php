<?php

namespace App\Http\Middleware;

use Closure;


class AppInstaller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // if (exist_file(thisApp('app_key')) === true):

        //     $database_status = test_database_connection();
        //     if ($database_status === true):
        //         if (get_file_content(thisApp('app_key')) == get_access_key() ):

        //             abort(404);

        //         endif;

        //     endif;            
            
        // endif;

        return $next($request);
        

    }
}
