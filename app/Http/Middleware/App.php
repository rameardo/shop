<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Session;
use Artisan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
class App
{
  
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /**
        * 
        * @var $database_status: check if database connected
        * @var $install_status: check if application is installed
        * 
        */
        $database_status = test_database_connection() === true ? true : false;
        $database_exist = check_database_exist();
        $install_status = false; // app is installed
        $key_status = false;
        $key = '';

        /**
        * 
        * @var $install_status: if status is false, do operations
        * 
        */
        if ($install_status === false):
            // Remove the cached bootstrap files
            Artisan::call('optimize:clear');
            // Flush the application cache
            Artisan::call('cache:clear');
            // Remove the configuration cache file
            Artisan::call('config:clear');

            // Create Application Key
            // create_file(thisApp('app_key'), generate_access_key());
            // $key = \Session::put('access_key', generate_access_key());


            $install_status = true;
        
        endif;

        

        // if ($install_status === true):
        //     create_file(thisApp('app_key_backup'), get_file_content(thisApp('app_key')));
        //     remove_file(thisApp('app_key'));
        // endif;

        // dd(check_database_exist());

        if ($install_status === false || $database_status === false):


            $installRoutes = array(
                route('install'),
                
                route('install.requirements'),
                
                route('install.info'),
                route('install.info.store'),

                route('install.database'),
                route('install.database.store'),

                route('install.admin'),
                route('install.admin.store'),

                route('install.done'),

                route('install.migrate'),
            );


            if (! in_array( $request->url(), $installRoutes)) {
                // return view('exception.app');
                return response()->make(view('thisApp::installer.welcome'), 404);
            }

        endif;



        // dd($database_exist);

        


        
        if ($install_status === true && $database_status === true):
            // dd(\Session::get($key));

            if ($database_exist === true):
                if (Schema::hasTable('app')):
                    if (get_access_key() === false):
                        $key = generate_access_key();
                        create_key(true, $key);
                        create_file(thisApp('app_key'), $key);
                    endif;
                endif;
            endif;
            
        

            // dd(get_file_content(thisApp('app_key_backup')));

            // return $next($request);

        endif;

        return $next($request);

    }
}
