<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if ($request->user() === null):
        //     return abort(404);
        // endif;

        // $actions = $request->route()->getAction();
        // $roles = isset($actions['roles']) ? $actions['roles'] : null;

        // if ($request->user()->hasAnyRole($roles) || !$roles):
        //     return $next($request);
        // endif;

        // return abort(404);

        // if (Auth::check()):

        //     if ($request->user() === null):
        //         abort(404);
        //     endif;

        //     if (currentUserLevel() == 1):
        //         return $next($request);
        //     endif;

        // endif;

        return $next($request);

        // abort(404);        

    }
}
