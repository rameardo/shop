<?php

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use App\Exceptions\CustomException;
use App\Exceptions\DupplicateException;
use App\Exceptions\RouteException;

use App\Model\Pub\Language;
use App\Model\Pub\Translation;
use App\Model\Pub\Notifcation;


if (!function_exists('thisApp'))
{
    function thisApp($value)
    {
        $app = array(
            'name'              => 'Snaiw',
            'support_email'     => 'info@shoppify.com',
            'support'           => 'http://support.shoppify.com',
            'version'           => 'v.11.1',
            'copyright'         => 'All copyright by me',
            'app_key'           => 'app/Providers/_key', // set Key Path
            'app_key_backup'    => 'vendor/_key', // set Key Path
            'config_path'       => '/app/Providers/',
            'view'              => 'bootstrap/Handler/views/'
        );

        if (!in_array($value, $app) || empty($value) || $value !== null):
            return $app[$value];
        else:
            return 'N/A';
        endif;

    }
}

if (!function_exists('errorCode'))
{
    function errorCode($value)
    {
        $app = array(
            'database'              => 5001,
            'database.exists'       => 5002,
            'database.dupplicate'   => 5003,
            'database.table.exists' => 5004,
            'database.notMigrated'  => 5005,
        );

        if (!in_array($value, $app) || empty($value) || $value !== null):
            return $app[$value];
        else:
            return 'N/A';
        endif;

    }
}




if (!function_exists('_sessionValue'))
{
    function _sessionValue($key)
    {
        return Session::get($key) !== null || !empty(Session::get($key)) ? Session::get($key) : '';
    }
}


if (!function_exists('_sessionSelected'))
{
    function _sessionSelected($key, $value)
    {
        return Session::get($key) !== null && Session::get($key) == $value ? 'selected' : '';
    }
}



if (! function_exists('dialog'))
{
	function dialog($status, $header, $message, $footer = null)
	{
        echo '<div class="modal fade" tabindex="-1" id="'. $status . '">';
            echo '<div class="modal-dialog" role="document">';
                echo '<div class="modal-content">';
                   echo '<a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>';
                    echo '<div class="modal-body modal-body-lg text-center">';
                        echo '<div class="nk-modal">';
                        	if ($status == 'success'):
                        		echo '<em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>';

                            elseif ($status == 'error'):
                            	echo '<em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>';

                            elseif ($status == "warning"):
                            	echo '<em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-alert-fill bg-warning"></em>';

                            else:
                            	echo '<em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-info-i bg-light"></em>';
                            	
                            endif;
                            echo '<h4 class="nk-modal-title">'. $header . '</h4>';
                            echo '<div class="nk-modal-text">';
                                echo '<div class="caption-text">'. $message .'</div>';
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';

                    if ($footer != null):
						// echo '<div class="modal-footer bg-lighter">';
                        echo '<div class="modal-footer bg-light">';
		                    echo '<div class="text-center w-100">';
		                        echo '<p>'. $footer .'</p>';
		                    echo '</div>';
		                echo '</div>';
		            endif;

	                echo '</div>';
            echo '</div>';
        echo '</div>';


	}
}


if (! function_exists('dialog_js'))
{
	function dialog_js($status)
	{
		echo "<script>";
			echo "$('#" . $status ."').modal('show');";
		echo "</script>";
	}
}






// Admin Prefix
if (!function_exists('ADMIN_PREFIX'))
{
    function ADMIN_PREFIX()
    {
        return 'admin';
    }
}

// Admin uri
if (!function_exists('ADMIN_URI'))
{
    function ADMIN_URI($uri = null)
    {
        return url('admin/' . $uri);
    }
}

// Request Uri
if (!function_exists('REQUEST_URI'))
{
    function REQUEST_URI($uri = '')
    {
        return 'admin/' . $uri;
    }
}

//  Menu Active
if (!function_exists('isMenuActive'))
{
    function isMenuActive($uri, $className)
    {
        return request()->is($uri) ? $className : '';
    }
}


// Get User Public IP
if (!function_exists('public_ip'))
{
    function public_ip()
    {
        if (!empty($_SERVER["HTTP_CLIENT_IP"]))
        {
        //check for ip from share internet
        $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
        // Check for the Proxy User
        $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        else
        {
        $ip = $_SERVER["REMOTE_ADDR"];
        }
        // This will print user's real IP Address
        // does't matter if user using proxy or not.
        return $ip;
    }
}



//  Number Converter
if (!function_exists('number_format'))
{
    function number_format( $n, $precision = 1 ) {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }
      // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
      // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }
        return $n_format . $suffix;
    }   
}

// Save Activity
if (!function_exists('logData'))
{
    function logData($userID, $signal, $fv=null, $fvl=null, $sv=null, $svl=null, $ai=null, $af=null, $au=null)
    {
        // New Activity object
        $activity = new Activity;

        // Activity Values
        $activity->user_id = $userID;
        $activity->signal = $signal;
        $activity->first_value = $fv;
        $activity->first_value_link = $fvl;
        $activity->second_value = $sv;
        $activity->second_value_link = $svl;
        $activity->activity_image = $ai;
        $activity->attached_file = $af;
        $activity->activity_url = $au;

        // Save the values
        $activity->save();
    }
}



// Get current language info
if (!function_exists('current_lang'))
{
    function current_lang($key=null)
    {
        $lang = Language::where('shortcut', App::getLocale())->get();

        if ($key === null or empty($key))
        {
            return $lang;
        }
        else
        {
            foreach ($lang as $langs) 
            {
                return $langs->$key;
            }           
        }

    }
}




// Language Function
if(!function_exists('_trans'))
{
    function _trans($key)
    {

        if (Translation::where([['key', '=', $key],['lang_shortcut', '=', App::getLocale()]])->count() > 0)
        {
            $trans = Translation::where([['key', '=', $key],['lang_shortcut', '=', App::getLocale()]])->get();

            foreach ($trans as $translation) 
            {
                if (empty($translation->value) or $translation->value === null)
                {
                    return 'N/L';
                }
                else
                {
                    return $translation->value;
                }
            }   
                        
        }

        else
        {
            $trans = Translation::where([['key', '=', $key],['lang_shortcut', '=', 'en']])->get();
            if (count($trans) > 0)
            {
                foreach ($trans as $translation) 
                {
                    if (empty($translation->value) or $translation->value === null)
                    {
                        return 'N/L';
                    }
                    else
                    {
                        return $translation->value;
                    }
                }   
            }
            else
            {
                return 'N/L';
            }
        }

    }
}


// Save Default Language Values
if (!function_exists('defaultLangKey'))
{
    function defaultLangKey($key, $value)
    {
    DB::table('translations')->insert([
        'lang_shortcut' => 'en',
        'key'                       => $key,
        'value'                 => $value,
        'created_at'        => now(),
        'updated_at'        => now()
    ]);     
    }
}


// Save Default Language Values
if (!function_exists('createRoute'))
{
    function createRoute($http_method, $url, $controller, $method, $name, $prefix, $status=null)
    {
        DB::table('routes')->insert([
            'http_method' => $http_method,
            'url'         => $url,
            'controller'  => $controller,
            'method'      => $method,
            'name'        => $name,
            'prefix'      => $prefix,
            'status'      => $status === null ? true : $status,
            'created_at'  => now(),
            'updated_at'  => now()
        ]);     
    }
}


if (! function_exists('admin_routes'))
{
    function admin_routes()
    {
        try {   
                
            $routes = DB::table('routes')->where('prefix', adminPrefix())->get();
            foreach($routes as $route):
                $method = $route->http_method;
                $url = $route->url;
                $controller = $route->controller . '@' . $route->method;
                $name = $route->name;
                $render = "Route::$method('$url', '$controller')->name('$name');";
                // return ;
                echo nl2br(eval($render)); 
            endforeach;

        } catch (Exception $e) {
            throw new RouteException('Something wrong with fetching admin routes ' . $e->getMessage());
            // abort(500);
        }
    }
}


if (! function_exists('adminPrefix'))
{
    function adminPrefix()
    {
        try {

            $prefix = select_record('app_settings', 'name', 'admin_prefix', 'value');
            return $prefix;

        } catch (Exception $e) {
            dd('Error fetching prefix');
        }
    }
}


if (!function_exists('_setRoute'))
{
    function _setRoute($key)
    {
        // if (_existTable('routes') == true):

        //     $route = DB::table('routes')->select('name')->where('key', $key)->get();

        //     if (count($route) > 0):

        //         foreach($route as $r):
                
        //             return $r->name;

        //         endforeach;

        //     else:
        //         return false;

        //     endif;

        // endif;

        // try {

            
        // } catch (Exception $e) {
        //     // dd('Route not Found');
        //     throw new RouteException;
        // }

    }
}

if (!function_exists('_existTable'))
{
    function _existTable($table)
    {
        $table = \Schema::hasTable($table);

        return $table;

    }
}



if(!function_exists('get_ip'))
{
    function get_ip()
    {
        if(isset($_SERVER['HTTP_CLIENT_IP']))
        {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
        }
    }
}




if(!function_exists('visitors'))
{
    function visitors($key=null)
    {
        $ip = '213.162.80.249';
        // $ip = get_ip();
        $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));

        if ($key === null || empty($key))
        {
            foreach ($query as $key => $value) {
                echo $value . ' <br>';
            }
        }
        else
        {
            if ($query && $query['status'] == 'success')
            {

                if (in_array($ip, $query)) {
                    return $query[$key];
                }
                
                else
                {
                    return 'N/A';
                }

            }
            else
            {
                return 'N/A';
            }           
        }

    }
}


if(!function_exists('_can'))
{
    function _can($value = null)
    {

        if (Auth::check()):

            // Current user permissions
            $permissions = Auth::user()->permissions;
            return $permissions->$value;

            

        endif;

    }
}



if (!function_exists('currentUserLevel'))
{
    function currentUserLevel()
    {
        if (Auth::check()):

            // Current user permissions
           $userLevel = Auth::user()->permissions;
           // $userLevel = DB::table('user_roles')->where('user_id', '=', Auth::user()->id)->first();

           dd($userLevel);
           return $userLevel;


        endif;
    }
}



if (!function_exists('currentUserRoleInformation'))
{
    function currentUserRoleInformation($value)
    {
        if (Auth::check()):

            // Current user permissions
           $userLevel = Auth::user()->permissions;

           return $userLevel->$value;


        endif;
    }
}

if (!function_exists('_setEnv'))
{
    function _setEnv(array $values)
    {
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);

        if (count($values) > 0) {
            foreach ($values as $envKey => $envValue) {

                $str .= "\n"; // In case the searched variable is in the last line without \n
                $keyPosition = strpos($str, "{$envKey}=");
                $endOfLinePosition = strpos($str, "\n", $keyPosition);
                $oldLine = substr($str, $keyPosition, $endOfLinePosition - $keyPosition);

                // If key does not exist, add it
                if (!$keyPosition || !$endOfLinePosition || !$oldLine) {
                    $str .= "{$envKey}={$envValue}\n";
                } else {
                    $str = str_replace($oldLine, "{$envKey}={$envValue}", $str);
                }

            }
        }

        $str = substr($str, 0, -1);
        if (!file_put_contents($envFile, $str)) return false;
        return true;
    }

}







// Error Handler 


if (! function_exists('check_database_connection'))
{
    function check_database_connection()
    {

        try {
            DB::connection()->getPdo();
            return true;
        } catch (\Throwable $e) {
            return false;
            // throw new CustomException($e->getMessage());
        }
        
    }
}

if (! function_exists('test_database_connection'))
{
    function test_database_connection()
    {
        try{
            $dsn = env('DB_HOST') . ':' . env('DB_PORT');
            $dbh = new pdo( "mysql:host={$dsn};dbname=",
                            env('DB_USERNAME'),
                            env('DB_PASSWORD'),
                            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            return true;
        }
        catch(PDOException $ex){
            return false;
        }
        
    }
}

if (! function_exists('create_database'))
{
    function create_database($database)
    {
        $host = env('DB_HOST');
        $root = env('DB_USERNAME');
        $root_password = env('DB_PASSWORD');
        try {
            $dbh = new PDO("mysql:host=$host", $root, $root_password);

            $dbh->exec("CREATE DATABASE IF NOT EXISTS `$database`");

            return true;

        } catch (PDOException $e) {
            // die("DB ERROR: ". $e->getMessage());
            return false;
        }
    }
}



if (! function_exists('check_database_exist'))
{

    function check_database_exist()
    {
        // $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
        // $db = DB::select($query, [env('DB_DATABASE')]);
        // $db = DB::statement("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'db_1'");
        // if (!$db) {
        //     return false;
        // } else {
        //     return true;
        // }        

        try {
            $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
            $db = DB::select($query, [env('DB_DATABASE')]);
            if (!empty($db)):
                return true;
            endif;
        } catch (Exception $e) {
            return false;   
        }

    }

}





if (! function_exists('generate_access_key'))
{
    function generate_access_key()
    {
        $app = strtolower(thisApp('name')) . ':';
        return  $app. bin2hex(random_bytes(16));
    }
}

if (! function_exists('create_key'))
{
    function create_key($permission=null, $key)
    {
        if ($permission === true):
            DB::table('app')->insert([
                'name'        => 'Key',
                'value'       => $key,
                'status'      => true,
                'created_at'  => now(),
                'updated_at'  => now()             
            ]);
            // _setEnv([
            //     'APP_ACCESS'    => env('APP_ON_ACCESS')
            // ]);

        endif;

    }
}

if (! function_exists('get_access_key'))
{
    function get_access_key()
    {
        $database_status = test_database_connection() === true ? true : false;
        if ($database_status === true):
            $key = DB::table('app')->where('name', 'Key')->first();
            // return $key->value;
            if ($key):
                return $key->value;
            else:
                return false;
            endif;
        endif;
    }
}


if (! function_exists('craete_file'))
{
    function create_file($path, $content)
    {
        $fp = fopen(base_path() . '/' .$path,"wb");
        fwrite($fp,$content);
        fclose($fp);        
    }
}

if (! function_exists('remove_file'))
{
    function remove_file($path)
    {
        if (exist_file($path)){
            unlink(base_path() . '/' . $path);
        }       
        else {
            return false;
        }
    }
}


if (! function_exists('get_file_content'))
{
    function get_file_content($path)
    {
        // $response = base_path() . '/' . $path;
        // $str2 = str_replace ('\\', '/', $response);

        // $file = file_get_contents( $str2, true);     
        // return $file;

        $file = file_get_contents(base_path() . '/' . $path , true);     
        return $file;

    }
}

if (! function_exists('exist_file'))
{
    function exist_file($filename)
    {

        if (file_exists(base_path() . '/' . $filename)) {
            return true;
        } else {
            return false;
        }        

    }
}


if (!function_exists('createSetting'))
{
    function createSetting($name, $description, $value)
    {
        try{
            DB::table('app_settings')->insert([
                'name'         => $name,
                'description'  => $description,
                'value'        => $value,
                'created_at'   => now(),
                'updated_at'   => now()
            ]);     
        } catch (Exception $e) {
            $message = " $name $value already exists.";
            throw new DupplicateException($message, 500);
        }             
    }
}


if (!function_exists('setApp'))
{
    function setApp($name, $value, $status)
    {
        try{
            DB::table('app')->insert([
                'name'        => $name,
                'value'       => $value,
                'status'      => $status,
                'created_at'  => now(),
                'updated_at'  => now()
            ]);     
        } catch (Exception $e) {
            $message = " $name $value already exists.";
            throw new DupplicateException($message, 500);
        }        
    }
}


if (!function_exists('createAdminAccount'))
{
    function createAdmin($first_name, $last_name, $email, $password)
    {
        // throw new DupplicateException;
        try {
            DB::table('users')->insert([
                'first_name'  => $first_name,
                'last_name'   => $last_name,
                'email'       => $email,
                'password'    => Hash::make($password),
                'created_at'  => now(),
                'updated_at'  => now()
            ]);              
        } catch (Exception $e) {
            // report($e);
            $message = "User $first_name $last_name already exists.";
            throw new DupplicateException($message, 500);
            // dd($e->getMessage());
        }
   
    }
}








// Database Records

if (! function_exists('select_record'))
{
    function select_record($table, $select, $where, $data)
    {
        try {
            $rec = DB::table($table)->where($select, $where)->first();
            return $rec->$data;
        } catch (Exception $e) {
            // dd('Error Selected ' . $select . '/' . $where . '/' . $data . '->' . $e->getMessage());
            return '[N/A select_record()]';
            // abort(500);
        }
    }
}


if (!function_exists('countRecord'))
{
    function ajaxCount($table)
    {
            try {

                if (\Schema::hasTable($table)):
                
                    $total = DB::table($table)->get();
                    return count($total);
                
                else:
                    return false;
                
                endif;


            } catch (Exception $e) {
                return false;
            }
        
    }
}


if (! function_exists('websiteUrl'))
{
    function websiteUrl()
    {
        return request()->getSchemeAndHttpHost();
    }
}


if (! function_exists('tableExist'))
{
    function tableExist($table)
    {
        if (Schema::hasTable($table)):
            return true;
        else:
            return false;
        endif;
    }
}




if (! function_exists('notifcation'))
{
    function notifcation(array $values)
    {
        $notifcation = new Notifcation();
        $cols = array('user_id', 'role_id', 'title', 'descriptions', 'link', 'where', 'status');
        try {
            if (test_database_connection() === true):
                if (Schema::hasTable('notifcations')):
                    foreach ($values as $coloumn => $value):
                        // $key = coloumn of Database
                        // $value : Value of Database
                        
                        if (in_array($coloumn, $cols)):
                            
                            $notifcation = new Notifcation();

                            $notifcation->$coloumn = $value;

                            $notifcation->save();

                        else:
                            dd('function notication(): \'coloumn\' ' . $coloumn . ' does not exist in table notifcations');
                        endif;
                    endforeach;

                else:
                    dd('Table notifcation not exist');

                endif;

                
            endif;

            
        } catch (Exception $e) {
            dd('something wrong is hppend with (notifcation function): ' . $e->getMessage());
        }


    }
}






// Dynamic Database Query

if (! function_exists('insertQuery'))
{
    function insertQuery($table, $column, $column_name, $column_value, $value)
    {
        return DB::table($table)
                ->where($column, $column_name)
                ->update([$column_value => $value]);
    }
}


if (! function_exists('submitStatus'))
{
    function submitStatus($status, $message = "Success")
    {

        if ($status === true):

            return response()->json(['message' => $message, 'status' => true], 200);

        else:

            return response()->json(['message' => $message, 'status' => false], 404);

        endif;

    }
}