<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Pub\Category;


class PagesController extends Controller
{
    public function index()
    {

        $categories = Category::where('parent_id', null)->get();

        return view('front.root',compact('categories'));

    }



}
