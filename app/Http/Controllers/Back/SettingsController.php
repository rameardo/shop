<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class SettingsController extends Controller
{
    public function generalSettings(Request $request)
    {


        return view('back.settings.general.index');

    }

    public function contactSettings()
    {
        return view('back.settings.general.contact_settings');
    }


}
