<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Model\Pub\Category;

class PagesController extends Controller
{
	// Dashboard
    public function Dashboard(Request $request)
    {
        
  

        // if($request->ajax()):
        //     $data = $request->data;

        //     // return response()->json(['value' => ajaxCount($request->table) !== false ? ajaxCount($request->table) : 'Table' . $request->table . ' does not exist']);
        //     return response()->json($data);


        // endif;

   

        if($request->ajax()):
            $translations = $request->translations;

            // $current_count = DB::table($name)->count();
            
            return response()->json($translations);


        endif;

 

    	return view('back.root');
    }

    // Category
    public function Category()
    {
        $categories = Category::paginate(12, ['*'], 'pagew');

        $total = count(Category::all());
        $deleteID = '';
        
    	return view('back.categories.index', compact('categories', 'total', 'deleteID'));
    }
}
