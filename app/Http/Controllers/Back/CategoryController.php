<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Http\Requests\CategoryRequest;


use App\Model\Pub\Category;

class CategoryController extends Controller
{
    public function New()
    {
    	$categories = Category::where('parent_id', null)->get();
    	return view('back.categories.new', compact('categories'));
    }

    public function Create(CategoryRequest $request)
    {

    	// New object from Category model
    	$category = new Category();

        // Validation


    	// Category name & parent
    	$category->name = $request->name;
    	$category->parent_id = $request->parent_id;

    	// Properties
    	$category->visibility = $request->visibility ? 1 : 0;
    	$category->show_in_homepage = $request->show_in_homepage ? 1 : 0;
    	$category->show_in_search = $request->show_in_search ? 1 : 0;
    	$category->show_category_image = $request->show_category_image ? 1 : 0;
    	$category->order = $request->order;
    	$category->homepage_order = $request->homepage_order;

    	//SEO Information
    	$category->title = $request->title;
    	$category->slug = $request->slug;
    	$category->slug = empty($category->slug) || $category == '' ? Str::slug($category->name, '-') : Str::slug($request->slug, '-');
    	$category->description = $request->description;

    	// Store request data
    	$category->save();

    	// Send confirmation message
    	$success_message = "Category {$category->name} Successfully created!";

    	// redirect to category page
    	return redirect('/admin/categories')->with('success', $success_message);

    }


    public function Destroy(Request $request)
    {
        // Get category id
        $id = $request->id;

        // find id
        $category = Category::find($id);

        // Messages
        
        
        
        // check if this category exist in database
        if ($category != null):
            $success_message = "Category {$category->name} Successfully deleted!";
            $category->delete();
            return redirect('/admin/categories')->with('success', $success_message);

        else:
            $error_message = 'Something wrong happend!';
            return redirect('/admin/categories')->with('error', $error_message);
        endif;

 
    }


    public function Details($category)
    {
        $category = Category::where('slug', $category)->first();

        if ($category):
            return view('back.categories.details', compact('category'));

        else:
            return redirect('/admin/categories');
        endif;
        
    }


}
