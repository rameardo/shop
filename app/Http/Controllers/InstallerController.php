<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Console\Output\StreamOutput;
use Artisan;
use Session;
use Illuminate\Support\Str;
use DB;
use Illuminate\Support\Facades\Hash;

class InstallerController extends Controller
{


    public function index()
    {
    	return view('thisApp::installer.index');

    }

    public function requirements()
    {


        $latestLaravelVersion = '7.0';

        $laravelVersion = (isset($_GET['v'])) ? (string)$_GET['v'] : $latestLaravelVersion;

        if (!in_array($laravelVersion, array('4.2', '5.0', '5.1', '5.2', '5.3', '5.4', '5.5', '5.6', '5.7', '5.8', '6.0', '7.0'))) {
            $laravelVersion = $latestLaravelVersion;
        }


        $laravel42Obs = 'As of PHP 5.5, some OS distributions may require you to manually install the PHP JSON extension.
        When using Ubuntu, this can be done via apt-get install php5-json.';
        $laravel50Obs = 'PHP version should be < 7. As of PHP 5.5, some OS distributions may require you to manually install the PHP JSON extension.
        When using Ubuntu, this can be done via apt-get install php5-json';

        $reqList = array(
            '4.2' => array(
                'php' => '5.4',
                'mcrypt' => true,
                'pdo' => false,
                'openssl' => false,
                'mbstring' => false,
                'tokenizer' => false,
                'xml' => false,
                'ctype' => false,
                'json' => false,
                'obs' => $laravel42Obs
            ),
            '5.0' => array(
                'php' => '5.4',
                'mcrypt' => true,
                'openssl' => true,
                'pdo' => false,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => false,
                'ctype' => false,
                'json' => false,
                'obs' => $laravel50Obs
            ),
            '5.1' => array(
                'php' => '5.5.9',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => false,
                'ctype' => false,
                'json' => false,
                'obs' => ''
            ),
            '5.2' => array(
                'php' => array(
                    '>=' => '5.5.9',
                    '<' => '7.2.0',
                ),
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => false,
                'ctype' => false,
                'json' => false,
                'obs' => ''
            ),
            '5.3' => array(
                'php' => array(
                    '>=' => '5.6.4',
                    '<' => '7.2.0',
                ),
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => false,
                'json' => false,
                'obs' => ''
            ),
            '5.4' => array(
                'php' => '5.6.4',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => false,
                'json' => false,
                'obs' => ''
            ),
            '5.5' => array(
                'php' => '7.0.0',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => false,
                'json' => false,
                'obs' => ''
            ),
            '5.6' => array(
                'php' => '7.1.3',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => true,
                'json' => true,
                'obs' => ''
            ),
            '5.7' => array(
                'php' => '7.1.3',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => true,
                'json' => true,
                'obs' => ''
            ),
            '5.8' => array(
                'php' => '7.1.3',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => true,
                'json' => true,
                'obs' => ''
            ),
            '6.0' => array(
                'php' => '7.2.0',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => true,
                'json' => true,
                'bcmath' => true,
                'obs' => ''
            ),
            '7.0' => array(
                'php' => '7.2.5',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => true,
                'json' => true,
                'bcmath' => true,
                'obs' => ''
            ),
        );


        $strOk = '<em class="icon ni ni-check-circle-fill text-success"></em> ';
        $strFail = '<em class="icon ni ni-cross-circle-fill text-danger"></em>';
        $strUnknown = '<em class="icon ni ni-alert-fill text-warning" ></em>';

        $requirements = array();


        // PHP Version
        if (is_array($reqList[$laravelVersion]['php'])) {
            $requirements['php_version'] = true;
            foreach ($reqList[$laravelVersion]['php'] as $operator => $version) {
                if ( ! version_compare(PHP_VERSION, $version, $operator)) {
                    $requirements['php_version'] = false;
                    break;
                }
            }
        }else{
            $requirements['php_version'] = version_compare(PHP_VERSION, $reqList[$laravelVersion]['php'], ">=");
        }

        // OpenSSL PHP Extension
        $requirements['openssl_enabled'] = extension_loaded("openssl");

        // PDO PHP Extension
        $requirements['pdo_enabled'] = defined('PDO::ATTR_DRIVER_NAME');

        // Mbstring PHP Extension
        $requirements['mbstring_enabled'] = extension_loaded("mbstring");

        // Tokenizer PHP Extension
        $requirements['tokenizer_enabled'] = extension_loaded("tokenizer");

        // XML PHP Extension
        $requirements['xml_enabled'] = extension_loaded("xml");

        // CTYPE PHP Extension
        $requirements['ctype_enabled'] = extension_loaded("ctype");

        // JSON PHP Extension
        $requirements['json_enabled'] = extension_loaded("json");

        // Mcrypt
        $requirements['mcrypt_enabled'] = extension_loaded("mcrypt_encrypt");

        // BCMath
        $requirements['bcmath_enabled'] = extension_loaded("bcmath");

        // mod_rewrite
        $requirements['mod_rewrite_enabled'] = null;

        if (function_exists('apache_get_modules')) {
            $requirements['mod_rewrite_enabled'] = in_array('mod_rewrite', apache_get_modules());
        }



        return view('thisApp::installer.requirements', compact('requirements', 'reqList', 'laravelVersion', 'strOk', 'strFail', 'strUnknown'));
    }

    public function info()
    {
        return view('thisApp::installer.info');
    }

    public function storeInfo(Request $request)
    {
        $validatedData = $request->validate([
            'app_name' => 'required|max:255',
            'description' => 'required',
            'environment' => 'required',
            'debug' => 'required',
        ]);
        Session::put('app_name', !empty($request->app_name) ? $request->app_name : '');
        Session::put('description', !empty($request->description) ? $request->description : '');
        Session::put('environment', empty($request->environment[0]) || $request->environment[0] === null ? '' : $request->environment[0]);
        Session::put('debug', empty($request->debug[0]) || $request->debug[0] === null ? '' : $request->debug[0]);

        return redirect(route('install.database'));

    }


    public function database()
    {
        return view('thisApp::installer.database');
    }

    public function storeDatabase(Request $request)
    {
        $validatedData = $request->validate([
            'db_driver' => 'required',
            'db_name' => 'required|max:255',
            'db_hostname' => 'required|max:255|ipv4',
            'db_port' => 'required|numeric|min:1|max:65535',
            'db_username' => 'required|max:255',
        ]);        

        Session::put('db_driver', empty($request->db_driver[0]) || $request->db_driver[0] === null ? '' : $request->db_driver[0]);
        Session::put('db_name', !empty($request->db_name) ? $request->db_name : '');
        Session::put('db_hostname', !empty($request->db_hostname) ? $request->db_hostname : '');
        Session::put('db_port', !empty($request->db_port) ? $request->db_port : '');
        Session::put('db_username', !empty($request->db_username) ? $request->db_username : '');
        Session::put('db_password', !empty($request->db_password) ? $request->db_password : '');

        $db_driver = Session::get('db_driver');
        $db_name = Session::get('db_name');
        $db_hostname = Session::get('db_hostname');
        $db_port = Session::get('db_port');
        $db_username = Session::get('db_username');
        $db_password = Session::get('db_password');

        // create_file(thisApp('config_path') . '_db_driver', $db_driver);
        // create_file(thisApp('config_path') . '_db_hostname', $db_hostname);
        // create_file(thisApp('config_path') . '_db_port', $db_port);
        // create_file(thisApp('config_path') . '_db_name', $db_name);
        // create_file(thisApp('config_path') . '_db_username', $db_username);
        // create_file(thisApp('config_path') . '_db_password', $db_password);

        return redirect(route('install.admin'));

    }



    public function admin()
    {
        return view('thisApp::installer.admin');

    }



    public function storeAdmin(Request $request)
    {

        $validatedData = $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email_address' => 'required|max:255|email',
            'password' => 'required|max:255|min:8',
            'maintenance_mode' => 'required',
            'registration' => 'required',
            'seed_data' => 'required',
        ]);        

        Session::put('first_name', !empty($request->first_name) ? $request->first_name : '');
        Session::put('last_name', !empty($request->last_name) ? $request->last_name : '');
        Session::put('email_address', !empty($request->email_address) ? $request->email_address : '');
        Session::put('password', !empty($request->password) ? $request->password : '');
        Session::put('maintenance_mode', empty($request->maintenance_mode[0]) || $request->maintenance_mode[0] === null ? '' : $request->maintenance_mode[0]);
        Session::put('registration', empty($request->registration[0]) || $request->registration[0] === null ? '' : $request->registration[0]);
        Session::put('seed_data', empty($request->seed_data[0]) || $request->seed_data[0] === null ? '' : $request->seed_data[0]);

        return redirect(route('install.done'));


    }



    public function done()
    {
        


        $app_name = Session::get('app_name');
        $description = Session::get('description');
        $environment = Session::get('environment') == 1 ? 'Poductiv' : 'Local';
        $debug = Session::get('debug') == 1 ? 'true' : 'false';

        $db_driver = Session::get('db_driver');
        $db_name = Str::slug(Session::get('db_name'), '_');
        $db_hostname = Session::get('db_hostname');
        $db_port = Session::get('db_port');
        $db_username = Session::get('db_username');
        $db_password = Session::get('db_password');

        $first_name = Session::get('first_name');
        $last_name = Session::get('last_name');
        $email_address = Session::get('email_address');


        // // Set data in .env


        // Set Application information Information
        // _setEnv([
        //     'APP_NAME' => Str::slug($app_name, '_'), 
        //     'APP_DESCRIPTION' => Str::slug($description, '_'), 
        //     'APP_ENV' => $environment, 
        //     'APP_DEBUG' => $debug, 
        // ]);


        // Set Database Information
        _setEnv([
            'DB_CONNECTION' => $db_driver, 
            'DB_HOST' => $db_hostname, 
            'DB_PORT' => $db_port, 
            'DB_DATABASE' => $db_name, 
            'DB_USERNAME' => $db_username, 
            'DB_PASSWORD' => $db_password, 
        ]);


        // Results Data
        // $db_status = check_database_connection() === true ? true : false;
        $db_status = test_database_connection() === true ? true : false;
        $db_exists = check_database_exist();
        $db_creation = false;

        if ($db_exists === true):
            $error_message = '

            <p class="text-left">
                The database <strong><u>' . env('DB_DATABASE') .'</u></strong> already exists. Please fiel a database name that does not exist in <strong><u>'. env('DB_CONNECTION') .'</u></strong> in order to create and install the application.
                              
            </p> 


            <p class="sub-text-sm text-left">Error code: '.errorCode('database.exists') .'</p>

            ';            
            return redirect(route('install.database'))->with('connection_error', $error_message);
        endif;
     
        // Handle Database connection
        if ($db_status === true):
            $db_creation = true;
            // Artisan::call("db:create {$db_name}");
            create_database($db_name);

        elseif ($db_status === false):
            $error_message = '

            <p class="text-left">
                There is a mistake in your database configuration, please review the following points
                <ul class="list list text-left">
                    <li>Check if the database is operator</li>
                    <li>Check the type of database for example is mysql or SQLite etc..</li>
                    <li>Check username and password for the databases</li>
                    <li>Check the database hostname/ip address and port</li>
                </ul>                
            </p> 

            <p class="sub-text-sm text-left">After reviewing the mentioned points, reinstall the application.</p>
            <p class="sub-text-sm text-left">Error code: '.errorCode('database') .'</p>

            ';
            return redirect(route('install.database'))->with('connection_error', $error_message);            
        endif;

        
        return view('thisApp::installer.done', compact([
            'app_name', 'description', 'environment', 'debug', 
            'db_driver', 'db_name', 'db_hostname', 'db_port',
            'first_name', 'last_name', 'email_address', 
            'db_status', 'db_creation'
        ]));

        
    }

    public function migrate()
    {

            Artisan::call('migrate');
            Artisan::call('db:seed');

            // Save Application Data
            setApp('Name', Session::get('app_name'), false);
            setApp('Description', Session::get('description'), false);
            setApp('Version', thisApp('version'), false);
            createSetting('Debug', 'Application Debugging Status', Session::get('debug') == 1 ? true : false);
            createSetting('Environment', 'Application Environment Status', Session::get('environment') == 1 ? true : false);
            createSetting('Maintenance', 'Application Maintenance Mode', Session::get('maintenance_mode') == 1 ? true : false);
            createSetting('Registration', 'Application Registration Mode', Session::get('registration') == 1 ? true : false);
            createSetting('admin_prefix', 'This is Admin Prefix', 'admin');
            createAdmin(Session::get('first_name'), Session::get('last_name'), Session::get('email_address'), Session::get('password'));


            // Remove all Sessions
            Session::forget([
                'app_name', 'description', 'environment', 'debug',
                'db_driver', 'db_hostname', 'db_port', 'db_name', 'db_username', 'db_password',
                'first_name', 'last_name', 'email_address', 'password', 'maintenance_mode', 'registration', 'seed_data'
            ]);

            return redirect(route('root'));            

    }


    // public function setNewAccessKey(Request $request)
    // {
        
    // }
    
}
