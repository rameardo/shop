<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:6|unique:categories',
            'title' => 'required',
            'slug' => 'unique:categories',
            'order' => 'digits_between:0,1000',
            'homepage_order' => 'digits_between:0,1000',
            'description' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'name.min' => 'The :attribute must be at least :min characters.',
        ];
    }


}
