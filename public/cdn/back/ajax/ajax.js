function currentValue(value)
{
    return $("#" + value).text();
}
function backendCheckCounter(counter, element)
{
    if (counter < 9 && counter > 0) 
    {
      $(element).append('<span class="bg-danger notifcation-count">' + counter + '</span>');
    }
    else if (counter > 9)
    {
      $(element).append('<span class="bg-danger notifcation-count">+9</span>');
    }
    else
    {
      $(element).children('span').remove();
    }     
}

function ajax(url, method = 'POST', data, callback)
{


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });    

    $.ajax({
        url: url,
        method: method,
        data: data,   

        headers: {
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded;",
        },

        beforeSend: function ()
        {

        },

        error: function (response)
        {
            switch (true) {
                case typeof response.responseText !== 'undefined':
                    console.log(response.responseText);
                    break;
                case response.statusText !== 'abort' && e.status === 0:
                    alert('Please check your internet connection');
                    break;
                case response.statusText === 'abort':
                    break;
                default:
                    console.error(response);
            }
        },

        complete: function (response)
        {
            // console.log('done');
            callback(response);
        }
    }
    );

}


function syncElement(element, updateElement, table, type, second)
{

    console.log('Started');

    $.get('/html/content-loader.html', function(update){
        $(updateElement).append(update);
    });
    setTimeout(function(){
        console.log('Test');
        $(updateElement).children(".content-loader").remove();
    }, 100);

    

    var ajax_call = function()
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({

            headers: {
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded;",                
            },

            url: website_url + '/ajax/sync',
            method: 'GET',
            data: {
                table: table,
                type: type
            },

            error: function (response)
            {
                switch (true) {
                    case typeof response.responseText !== 'undefined':
                        console.log(response.responseText);
                        break;
                    case response.statusText !== 'abort' && e.status === 0:
                        alert('Please check your internet connection');
                        break;
                    case response.statusText === 'abort':
                        break;
                    default:
                        console.error(response);
                }
            },


            complete: function(response)
            {

                if (response.readyState == 4 && response.status == 200)
                {
                    // extract to html
                    $(element).html(response.responseJSON.count);
                    
                }
                // console.log(response);
                
                
            }

        });
    }

    // setTimeout(ajax_call, 10);
    setInterval(ajax_call, second);

}






function clickCount(viewElement, clickElement, table)
{

    $(clickElement).click(function(){


        $.get("html/content-loader.html", function (data) {
            $(".clickCount").append(data);
        });

        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

          $.ajax({

            headers: {
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded;",
            },

            url: website_url + '/ajax/count/' + table,
            method: 'GET',
            data: table,

            error: function (response)
            {
                switch (true) {
                    case typeof response.responseText !== 'undefined':
                        console.log(response.responseText);
                        break;
                    case response.statusText !== 'abort' && e.status === 0:
                        alert('Please check your internet connection');
                        break;
                    case response.statusText === 'abort':
                        break;
                    default:
                        console.error(response);
                }
            },

            complete: function(response)
            {
                
                $(".clickCount").children(".content-loader").remove();

                if (response.status == 200 && response.readyState == 4)
                {
                    $(viewElement).text(response.responseText);
                    // console.log(response);
                }
                else
                {
                    $(viewElement).html('<p class="nk-wg5-foot text-soft">ERR: 5004, Table: ' + table + ' not found. <a href="' + current_url +'">Reload page?</a></p>');
                }
                
            }
          });   
            

    });

}


function backEndNotifcations()
{
       var ajax_call = function()
       {
            $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
           });    

          $.ajax({
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded;",
            },

            url: website_url + '/ajax/notifcations',
            method: 'GET',
            datatype: "json",
            success: backEndNotifcationsCallback,

          });   
      }   

    setInterval(ajax_call, 2000);
    // console.log('new counter:', counter);

}






function backEndNotifcationsCallback(response) {
  // var counter = 0;

  result = response;
  counter = result.total_unread;     
  notifcations = result.notifcations;      
  var toasts_signal = 0;
  

  // Count unread notifcations
  if (counter < 9 && counter > 0) 
  {
    $('#notifcationClick div').append('<span class="bg-danger notifcation-count">' + counter + '</span>');
  }
  else if (counter > 9)
  {
    $('#notifcationClick div').append('<span class="bg-danger notifcation-count">+9</span>');
  }
  else
  {
    $('#notifcationClick div').children('span').remove();
  }


  // Append Notifcations


   // console.log(counter);


}











function cNotifcations()
{
       var ajax_call = function()
       {
        var clickSignal;
            $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
           });    

          $.ajax({
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded;",
            },
            error: function (response)
            {
                switch (true) {
                    case typeof response.responseText !== 'undefined':
                        console.log(response.responseText);
                        break;
                    case response.statusText !== 'abort' && e.status === 0:
                        alert('Please check your internet connection');
                        break;
                    case response.statusText === 'abort':
                        break;
                    default:
                        console.error(response);
                }
            },

            url: website_url + '/ajax/notifcations',
            method: 'GET',
            datatype: "json",
            complete: function(response)
            {
                backendCheckCounter(response.responseJSON.total_unread, '#notifcationCounter');
                
                // fetch response
                // console.log(response);
            }
      

          });   
      }   

    setInterval(ajax_call, 10000);    
}



function bNotifcations()
{

    $.get("/html/content-loader.html", function (data) {
        $("#backend-notifcations-list").append(data);
    });

    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
   }); 


    $('#notifcationCounter').click(function(){

        $('#notifcationCounter span').remove();

        $.ajax({

            headers: {
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded;",
            },
            error: function (response)
            {
                switch (true) {
                    case typeof response.responseText !== 'undefined':
                        console.log(response.responseText);
                        break;
                    case response.statusText !== 'abort' && e.status === 0:
                        alert('Please check your internet connection');
                        break;
                    case response.statusText === 'abort':
                        break;
                    default:
                        console.error(response);
                }
            },

            url: website_url + '/ajax/notifcations/backend/click',
            type: 'GET',
            data: { clickSignal: 1 },
            datatype: 'html',
            complete: function(response)
            {

                if (response.status == 200 && response.readyState == 4)
                {

                    $('#backend-notifcations-list').html(response.responseText);

                    // Remove Loader
                    $(".backend-notifcations .dropdown-body").children(".content-loader").remove();                    
                }

                // fetch response
                // console.log(response);
            }
        });
    });


}

function backendNotifcation()
{
    cNotifcations();
    bNotifcations();

}



/*
    Example: submitUpdate('#contact-form', {
        'email' : 'fva-email', // required
        'phone_number': 'fva-phone_number:optional' // optional by adding:optional
    })
*/ 
function submitUpdate(form, table,  data, sendValues = {})
{

   $(form).on('submit', function(e){

        e.preventDefault();


        var form_validation = false;

        $.each(data, function( index, value ) {

          var element = value.split(':');

          // var element = $(value).val().split(':');

          // var s = {index : value};
          // s[index] = value;

          // sendValues[index] += value;
          // sendValues[index] = sendValues.push(value);
          // sendValues = {index + ':' + value};
          sendValues[index] = $('#' + element[0]).val();



          if (element[1] != 'optional')
          {
            var elementValue = $('#' + value).val();
            
            if (!elementValue)
            {
                form_validation = false;

            }
            else
            {
                form_validation = true;

            }
          }



          // console.log(element[0]);

         
        });


   
         if (form_validation === true)
         {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                
            $.ajax({

                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/x-www-form-urlencoded;",
                },
                error: function (response)
                {
                    switch (true) {
                        case typeof response.responseText !== 'undefined':
                            console.log(response.responseText);
                            break;
                        case response.statusText !== 'abort' && e.status === 0:
                            alert('Please check your internet connection');
                            break;
                        case response.statusText === 'abort':
                            break;
                        default:
                            console.error(response);
                    }
                },

                url: website_url + '/ajax/update/' + table,
                type: 'POST',
                data: { sendValues },
                complete: function(response)
                {
                    if (response.responseJSON.status === true)
                    {
                        console.log(response.responseJSON.message);
                    }
                    else if (response.responseJSON.status === false)
                    {
                        console.log(response.responseJSON.message);
                    }
                    else
                    {
                        console.log('Unknown error happend');
                    }
                    console.log(response);
                }
            });  

        }  

   });

}














// Run Functions
backendNotifcation();

