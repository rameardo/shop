// Installation

var BG = {}; 
// BAR GRAPH window object
// FIXED
BG.fixed = function(percentage, duration) {
  var pixels = Math.floor(percentage * 2.5); 
  // Percent as a whole number times 2.5 for 250 max pixels
	// Animate bar graph
	var count1 = 0,
      bar = $('.progress-fixed__bar'),
      interval1 = Math.floor(duration / pixels),
      incrementer1 = setInterval(function() {
        (count1 <= pixels) ? (bar.width(count1), count1++):clearInterval(incrementer1);
      }, interval1);
  // Animate percent number
  var count2 = 0,
      percent = $('.progress-fixed__percent'),
      interval2 = Math.floor(duration / percentage),
      incrementer2 = setInterval(function() {
        (count2 <= percentage) ? (percent.text(count2 + "%"), count2++):clearInterval(incrementer2);
      }, interval2);
};
// RESPONSIVE
BG.responsive = function(percentage, duration) {
  // Animate bar graph
  var count1 = 0,
      bar = $('.progress-responsive__bar'),
      interval1 = (Math.floor(duration / percentage) / 2),
      incrementer1 = setInterval(function() {
        (count1 <= percentage) ? (bar.width(count1 + "%"), count1 += 0.5):clearInterval(incrementer1);
      }, interval1);
  // Animate percent number
  var count2 = 0,
      percent = $('.progress-responsive__percent'),
      interval2 = Math.floor(duration / percentage),
      incrementer2 = setInterval(function() {
        (count2 <= percentage) ? (percent.text(count2 + "%"), count2++):clearInterval(incrementer2);
      }, interval2);
};
