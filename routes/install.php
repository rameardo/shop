<?php
use Illuminate\Support\Facades\Route;


Route::get('/', 'InstallerController@index')->name('install');

Route::get('/requirements', 'InstallerController@requirements')->name('install.requirements');

Route::get('/basic-info', 'InstallerController@info')->name('install.info');
Route::post('/basic-info/store', 'InstallerController@storeInfo')->name('install.info.store');

Route::get('/database', 'InstallerController@database')->name('install.database');
Route::post('/database/store', 'InstallerController@storeDatabase')->name('install.database.store');

Route::get('/admin', 'InstallerController@admin')->name('install.admin');
Route::post('/admin/store', 'InstallerController@storeAdmin')->name('install.admin.store');

Route::get('/done', 'InstallerController@done')->name('install.done'); 

Route::get('/report', 'InstallerController@migrate')->name('install.migrate'); 


// Route::post('/new-key', 'InstallerController@setNewAccessKey')->name('install.newKey'); 
