<?php
use Illuminate\Support\Facades\Route;


Route::get('/', 'Back\PagesController@Dashboard')->name('admin');
Route::get('/categories', 'Back\PagesController@Category')->name('admin.categories');
Route::get('/categories/new', 'Back\CategoryController@New')->name('admin.categories.new');
Route::get('/categories/{category}', 'Back\CategoryController@Details');
Route::post('/categories/create', 'Back\CategoryController@Create');
Route::post('/categories/destroy', 'Back\CategoryController@Destroy');

Route::get('/general-settings', 'Back\SettingsController@generalSettings')->name('admin.general_settings');
Route::get('/general-settings/contact-settings', 'Back\SettingsController@contactSettings')->name('admin.general_settings.contact_settings');
// Route::post('/settings/store', 'Back\SettingsController@generalSettingsStore')->name('admin.settings.store');

Route::get('/settings/test', 'Back\SettingsController@test')->name('admin.settings.test');

