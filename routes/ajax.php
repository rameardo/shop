<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

Route::middleware('Ajax')->group(function(){

	Route::get('/count/{table}', function($table)
	{

		$exists = tableExist($table);

		if ($exists === false):
			return response("Sorry could not found Table '$table' in Database, is '$table' correct name?", 404);
		elseif ($exists === true):

			return response(DB::table($table)->count(), 200);

		else:	
			return response('Error', 400);
		endif;

	});	


	Route::get('/notifcations', function()
	{
        
        $counter = DB::table('notifcations')
        		   ->where('where', 0)
        		   ->where('status', 1)
        		   ->count();

        return response()->json
        ([
        	'total_unread'  => $counter 
        	
        ]);
	});



	Route::get('/notifcations/backend/click', function(){

		if (request()->clickSignal == 1):
	        $backend_notifcations_unread = 
	                                DB::table('notifcations')
	                                ->where('where', 0)
	                                ->where('status', 1)
	                                ->get();


       		foreach ($backend_notifcations_unread as $notifcation):
       			DB::table('notifcations')
       				->where('where', 0)
				    ->where('status', 1)
				    ->update(['status' => 0]);
       			
       		endforeach;

	        $backend_notifcations = 
	                                DB::table('notifcations')
	                                ->where('where', 0)
	                                ->orderBy('created_at', 'DESC')
	                                ->get();


	        $counter = DB::table('notifcations')
	        		   ->where('where', 0)
	        		   ->count();

	        $output = '';


	        if ($counter > 0):	

	        $output .= '<div class="dropdown-body">';
	        $output .= '<div class="nk-notification">';

		        foreach ($backend_notifcations as $notifcation ):

		        	$link_status = strlen($notifcation->link) > 0 ?  $notifcation->link : '#' . \Str::slug($notifcation->title, '-') . 'at'. \Str::slug($notifcation->created_at, '-');

		        	$link = '<a class=nk-notification-content notifcation-content" href="'. $link_status .'">';

		        	$created_at = $notifcation->created_at; 
		        	$created_at = \Carbon\Carbon::parse($notifcation->created_at); 
		        	$date_created = $created_at->diffForHumans(['options' => \Carbon\Carbon::ONE_DAY_WORDS | \Carbon\Carbon::JUST_NOW | \Carbon\Carbon::NO_ZERO_DIFF]); 
		        
		        	$output .= '<div class="nk-notification-item dropdown-inner">';

		        	if ($notifcation->icon !== null):

		        		$output .= '<div class="nk-notification-icon">';
		        			$output .= '<em class="icon icon-circle' . $notifcation->icon . '"></em>';
		        		$output .= '</div>';

		        	else:

		        		$output .= '<div class="nk-notification-icon">';
		        			$output .= '<em class="icon icon-circle bg-indigo-dim ni ni-info-i"></em>';
		        		$output .= '</div>';

		        	endif;

		        		// if ()
		        		$output .= $link;

		        			if (strlen($notifcation->title) > 0):
		        				$output .= '<div class="nk-notification-text title fw-medium">' . $notifcation->title .'</div>';
		        			endif;
		        			
		        			if (strlen($notifcation->descriptions) > 0):
		        				$output .= '<div class="nk-notification-text descriptions text-muted">' . $notifcation->descriptions .'</div>';
		        			endif;
		        			
		        			
		        			if ($notifcation->created_at !== null ):
		        				$output .= '<div class="nk-notification-time">';

		        					// $output .= 'Hello';
		        					$output .= $created_at->diffForHumans(['options' => \Carbon\Carbon::ONE_DAY_WORDS | \Carbon\Carbon::JUST_NOW | \Carbon\Carbon::NO_ZERO_DIFF]);

		        				$output .= '</div>';
		        			endif;
		        			

		        			

		        		$output .= '</a>';
		        	$output .= '</div>';
		        
		        endforeach;
		    	$output .= '</div>';
		    	$output .= '</div>';

		    	$output .= '                        
		    		<div class="dropdown-foot center">
                         <a href="#">View All</a>
                    </div>';

	    	else:

	    		$output .= '
	    		<div class="no-notifcations">
	    			<img src="/cdn/back/icons/notifcations.svg">
	    			<p class="text-muted mt-1"> There are no Notifcations </p>
	    		</div>
	    		';

	    	endif;



       		return response($output, 200);

		endif;

		

	});





	Route::get('/sync', function(){

		$table = request()->table;
		$type = request()->type;

		// if Table is exist
		if (Schema::hasTable($table)):

			if ($type == 'count'):
				$count = DB::table($table)->count();
				return response()->json(['count' => $count], 200);
			endif;

		else:

			return response()->json(['error' => 'Table ' . $table . ' not exist in database.'], 404);

		endif;


	});


	Route::post('/update/{table}', function($table){

		// Get Requested Data
		$data = request()->sendValues;

		$total = 0; // Total of columns

		$status = false; // Save Status

		$statusMessage = ''; // Status Message 


		// If Table is exist
		if (Schema::hasTable($table)):

			foreach ($data as $coloumn => $value):


				$coloumn_s = explode('__', $coloumn); // Explode the string and converte it to variables by using __
				$col = $coloumn_s[0]; // coloumn name
				$col_name = $coloumn_s[1]; // first coloumn
				// $col_value = $coloumn_s[2]; // another coloumn

				$total = sizeof($coloumn_s);

				// if this column in $table exists, then force algorithm
				if (Schema::hasColumn($table, $col)):


					// check if $col_value has records, when yes, then insert records in table
					// if (isset($col_value) && strlen($col_value) > 0 && $col_value !== false):
					// if $total value more than one record, then set new value under $col_value and append coming data
					if ($total > 2):

						$col_value = $coloumn_s[2];

						// check if $col_value and $col is exist
						if (Schema::hasColumn($table, $col_value)):

							insertQuery($table, $col, $col_name, $col_value, $value);
							// return response()->json(['success' => true], 200);
							$status = true; // update status $variable
							$statusMessage = "true"; // update status Message variable

					    // when not exists response error message
						else:
							// return response()->json(['error' => "Column {$col_value} does not exists in table {$table}"], 404);
							$status = false; // update status $variable
							$statusMessage = "Column {$col_value} does not exists in table {$table}"; // update status Message variable

						endif;

					// if $col_value not exists
					else:

						insertQuery($table, $col, $col_name, $col, $value);
						// return response()->json(['success' => true], 200);
						$status = true; // update status $variable
						$statusMessage = "true"; // update status Message variable				 			

					endif;


				// if table in the database not exist
				else:
					// return response()->json(['error' => "Column {$col} does not exists in table {$table}"], 404);
					$status = false; // update status $variable
					$statusMessage = "Column {$col} does not exists in table {$table}";	// update status Message variable


				endif;



			endforeach;


			return submitStatus($status, $statusMessage); // response current status and status message to ajax

			// return $col_name;

		else: // If Table is not exists

			return response()->json(['error' => 'Table ' . $table . ' does not exist'], 404); // resonse error message that's $table in database not exist

		endif;


		

	});




}); // End of Middleware

