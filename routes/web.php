<?php
use Illuminate\Support\Facades\Route;
// use Clx\Xms\Client;
Route::get('lang/{lang}', 'LanguageController@index');


Route::get('/', 'Front\PagesController@index')->name('root');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');		
